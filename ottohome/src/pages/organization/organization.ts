import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ModalController, LoadingController, Events, MenuController } from 'ionic-angular';
import { DevicesPage } from '../devices/devices';
import { OfficedevicesPage } from '../officedevices/officedevices';
import { AppliancePage } from '../appliance/appliance';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Subscription } from 'rxjs/Subscription';
import { AddlevelsPage } from '../addlevels/addlevels';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { HTTP } from '@ionic-native/http';
import { MainPage } from '../main/main';


@Component({
  selector: 'page-organization',
  templateUrl: 'organization.html',
})
export class OrganizationPage {
  level: any;
  connected: Subscription;
  disconnected: Subscription;
  data_hierarchy: any;
  hierarchy: any;
  _id: any;
  devices = 'https://linked-things-events.eu-gb.mybluemix.net/api/v1/levels/';
  events: any;
  levelId: any;
  loading: any;
  hierarchyLevel: any;

  constructor( public menu: MenuController,public even:Events,private http: HTTP, private nativePageTransitions: NativePageTransitions, public loadingCtrl: LoadingController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private network: Network) {
    this.levelId = this.navParams.data
    this.data_hierarchy = JSON.parse(this.navParams.data._body)
    this.hierarchyLevel = this.data_hierarchy['levels'] 
    console.log(this.data_hierarchy);
    console.log(this.hierarchyLevel);
/*     console.log(this.hierarchy.Level._id);
    console.log(this.hierarchy.Level._id, 'levelid'); */
    this.menu.enable(true);

  }

  ionViewDidLoad() {
    this.even.publish('user:addorglevel');
    console.log('ionViewDidLoad OrganizationPage');
  }
  ionViewCanLeave() {
    this.even.publish('user:loggedout');

  }
  /*   showsubLevel(hierarchy){
      if(hierarchy['levels']){
        console.log(hierarchy['levels']);
        this.navCtrl.push(DevicesPage,hierarchy['levels'])
      }else{
        console.log(hierarchy['devices']);
        this.navCtrl.push(DevicesPage,hierarchy['devices'])
      }
  
    } */

  showsubLevel(data_hierarchy) {
    this._id = data_hierarchy._id
    console.log(data_hierarchy, 'obj')
    console.log(this._id, 'id')
    localStorage.setItem("levelId", this._id)
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    this.loading.present();
   
    // var user = localStorage.getItem('user');
    // let postParams = {
    //   // "Id": user.id,
    //   "token": user
    // }
    // var data = JSON.parse(postParams.token)
    // console.log(JSON.parse(data.data))
    // var datadata = JSON.parse(data.data)
    // console.log("here is token", datadata.token)
    // let headers = {
    //   'Authorization': 'Bearer ' + datadata.token
    // };
    /*     let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
        let options = new RequestOptions({ headers: headers }); */
    /* 
        this.http.get(this.devices + this._id + "/events", {}, headers)
          .then(data => {
            console.log(data.status);
            console.log(JSON.parse(data.data));
            this.level = JSON.parse(data.data) // data received by server
            console.log(data.headers);
    
          })
          .catch(error => {
    
            console.log(error.status);
            console.log(error.error); // error message as string
            console.log(error.headers);
    
          }); */

    if (data_hierarchy['levels']) {
      this.loading.dismiss();
      console.log(data_hierarchy['levels']);
      this.navCtrl.push(OfficedevicesPage, data_hierarchy['levels'])

    } else if (data_hierarchy['devices']) {
      console.log(data_hierarchy['devices']);

      this.loading.dismiss();
      this.navCtrl.push(DevicesPage, data_hierarchy['devices'])
    }
    else {
      console.log(data_hierarchy, 'before nav');
      this.loading.dismiss();
      this.navCtrl.push(MainPage, data_hierarchy)
    }
  }
  AddLevel() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
    const modal = this.modalCtrl.create(AddlevelsPage, this.levelId._id);
    modal.present();
  }
  ionViewDidEnter() {
    this.network.onConnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

    this.network.onDisconnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
  }
  displayNetworkUpdate(connectionState: string) {
    let networkType = this.network.type;
    this.toast.create({
      message: `You are now ${connectionState} via ${networkType}`,
      duration: 3000
    }).present();
  }
  ionViewWillLeave() {
    /*     this.connected.unsubscribe();
        this.disconnected.unsubscribe(); */
    /* var even = JSON.parse(localStorage.getItem('even'));
    localStorage.removeItem(localStorage.key(even.id)) */
  }
}
