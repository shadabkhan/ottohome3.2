import { Component, OnDestroy } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, ToastController } from 'ionic-angular';
import { AppliancePage } from '../appliance/appliance';
import { DevicesPage } from '../devices/devices';
import { DrcodereaderPage } from '../qrcode/drcodereader/drcodereader';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AddDevicesPage } from '../add-devices/add-devices';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Hotspot } from '@ionic-native/hotspot';
import { AddlevelsPage } from '../addlevels/addlevels';
import { MainPage } from '../main/main';
declare var WifiWizard;
@Component({
  selector: 'page-officedevices',
  templateUrl: 'officedevices.html',
})
export class OfficedevicesPage {
  data_hierarchy: any;
  _id: any;
  created: any;
  level: any;
  levelId: any;
  createdBy: any;
  levelsLimit: any;
  name: any;
  schema: any;
  type: any;
  updated: any;
  _rev: any;
  check: Array<any> = [];
  deviceCompareEvents: Array<any> = [];
  devices = 'https://linked-things-events.eu-gb.mybluemix.net/api/v1/levels/';
  events: any[];
  checkd: any[];
  newArray: any[];
  selectedProduct: any;
  pass: any;
  id: any;
  str: any;
  deviceId: any;
  value: any;
  hubId: any;
  loading: any;
    lenghtofDevices:   Array<any> = [];
  constructor(public toastCtrl: ToastController,private hotspot: Hotspot, private barcodeScanner: BarcodeScanner, private http: Http, private nativePageTransitions: NativePageTransitions, public loadingCtrl: LoadingController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
 /*    WifiWizard.getCurrentSSID(ssidHandler, fail)

    function ssidHandler(s) {
      console.log("Current SSID" + s);
      // JSON.stringify(localStorage.setItem("ssid",s))
      localStorage.setItem("ssid", JSON.parse(s))
    } function fail(e) {
      alert("Failed" + e);
    } */

    // console.log(this.newArray);
    /* 
        if (this.check.length > 0) {
          // this.data_hierarchy = this.navParams.data
          // console.log(this.data_hierarchy);
        
    
      
        } else if (!this.check.length) {
    
          this._id = this.navParams.data['_id'];
          this.created = this.navParams.data['created'];
          this.createdBy = this.navParams.data['createdBy'];
          this.level = this.navParams.data['level'];
          this.levelId = this.navParams.data['levelId'];
          this.levelsLimit = this.navParams.data['levelsLimit'];
          this.name = this.navParams.data['name'];
          this.schema = this.navParams.data['schema'];
          this.type = this.navParams.data['type'];
          this.updated = this.navParams.data['updated'];
          this._rev = this.navParams.data['_rev'];
        } */


  }
  ngOnInit() {
    console.log(this.navParams.data);
    this.check = this.navParams.data
    console.log(this.check);
    console.log(this.check);
    // for (var i = 0; i < this.check.length; i++) {
    //   console.log(this.check[i].devices.length);
    // //  this.lenghtofDevices = this.check[i].devices.length
    // this.lenghtofDevices.push(this.check[i].devices.length)
    // }
    // console.log(this.lenghtofDevices);
    this._id = this.navParams.data['_id'];
    this.deviceId = this.navParams.data['deviceId'];
    this.created = this.navParams.data['created'];
    this.createdBy = this.navParams.data['createdBy'];
    this.level = this.navParams.data['level'];
    this.levelId = this.navParams.data['levelId'];
    this.levelsLimit = this.navParams.data['levelsLimit'];
    this.name = this.navParams.data['name'];
    this.schema = this.navParams.data['schema'];
    this.type = this.navParams.data['type'];
    this.updated = this.navParams.data['updated'];
    this._rev = this.navParams.data['_rev'];
    this.value = this.navParams.data['value'];
    this.hubId = this.navParams.data['hubId'];
    localStorage.setItem('typedevice', this.type)
    console.log(this.hubId)
    console.log(this.type)
    console.log(this.value)
    console.log(this._id)
    // this.showsubLevel();
    // this.showmotorLevel();
    // this.emptyspace = 100 - this.value
    // console.log(this.emptyspace)
    console.log(this.type)
    localStorage.setItem("levelId._id", this._id)

    this.events = JSON.parse(localStorage.getItem('DeviceEvent'))
    // stack issue solve
    /*    for (var i = 0; i < this.events.length; i++) {
         // console.log(this.events)
         var ismatch = false;
         for (var j = 0; j < this.check.length; j++) {
           // console.log(this.check)
   
           if (this.events[i].deviceId == this.check[j]._id) {
             ismatch = true;
             console.log(this.check)
   
             this.deviceCompareEvents.push(this.events[i])
             console.log(this.deviceCompareEvents)
   
             localStorage.setItem("even", JSON.stringify(this.deviceCompareEvents))
             break;
             // console.log(this.newArray)
   
           }
         }
   
       } */

  }
  /*   showsubLevel(data_hierarchy) {
  
      console.log(data_hierarchy._id)
      localStorage.setItem("DeviecLevelId", data_hierarchy._id);
      var user = JSON.parse(localStorage.getItem('user'));
      let postParams = {
        // "Id": user.id,
        "token": user.token
      }
      let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
      let options = new RequestOptions({ headers: headers });
  
      this.http.get(this.devices + data_hierarchy._id + "/events", options)
        .subscribe(data => {
          data.json()
          this.events = data.json()
          localStorage.setItem("secDeviceEvent", JSON.stringify(this.events));
          console.log(this.events)
  
  
        }, (err) => {
          return err
        });
  
      if (data_hierarchy['levels']) {
        console.log(data_hierarchy['levels']);
        setTimeout(() => {
          this.navCtrl.push(DevicesPage, data_hierarchy['levels'])
        }, 3000);
      } else if (data_hierarchy['devices']) {
        console.log(data_hierarchy['devices']);
        setTimeout(() => {
          this.navCtrl.push(DevicesPage, data_hierarchy['devices'])
        },3000);
      }
      else {
        console.log(data_hierarchy);
        this.navCtrl.push(AppliancePage, data_hierarchy)
      }
    } */
  showsubLevel(data_hierarchy) {
    this._id = data_hierarchy._id
    localStorage.setItem("levelId", this._id)
    console.log(this._id, 'device level')
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    this.loading.present();
  
    /*    var user = localStorage.getItem('user');
       let postParams = {
         // "Id": user.id,
         "token": user
       }
      var data = JSON.parse(postParams.token)
       console.log(JSON.parse(data.data))
    var datadata = JSON.parse(data.data)
    console.log("here is token",datadata.token)
    let headers = {
      'Authorization': 'Bearer ' +datadata.token
   };
       this.http.get(this.devices + this._id + "/events",  {},headers)
         .then(data => {
           console.log(data.status);
           console.log(JSON.parse(data.data));
           this.level =JSON.parse(data.data) // data received by server
           console.log(data.headers);
       
         })
         .catch(error => {
       
           console.log(error.status);
           console.log(error.error); // error message as string
           console.log(error.headers);
       
         }); */
   /*  var user = localStorage.getItem('user');
    let postParams = {
      // "Id": user.id,
      "token": user
    }
    var data = JSON.parse(postParams.token)
    console.log(JSON.parse(data.data))
    var datadata = JSON.parse(data.data)
    console.log("here is token", datadata.token)
    let headers = {
      'Authorization': 'Bearer ' + datadata.token
    }; */
    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.devices + this._id + "/events",  options)
      .subscribe(data => {
        console.log( data.json())

        data.json()
       this.events =data.json()
        localStorage.setItem("DeviceEvent", JSON.stringify(this.events));
          this.loading.dismiss();
        console.log(this.events) 


      },(error) => {
        console.log(error)
        console.log(error)
        this.loading.dismiss();
   
        console.log(error.status, 1);
    
        if( error.status == 0){
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
      /*  else if ( error.status == 409) {
          let toast = this.toastCtrl.create({
            message: `email is has already been used`,
            duration: 2000
          });
          toast.present();
        }
        else if ( error.status == 401) {
          let toast = this.toastCtrl.create({
            message: `Invalid password`,
            duration: 2000
          });
          toast.present();
        }  else if ( error.status == 404) {
          let toast = this.toastCtrl.create({
            message: `user not found`,
            duration: 2000
          });
          toast.present();
        }  else if ( error.status == 400) {
          let toast = this.toastCtrl.create({
            message: `Only valid email will be chosen`,
            duration: 2000
          });
          toast.present();
        }  */
        else {
          // console.log(this.checkError.data[0].message);
  
          let toast = this.toastCtrl.create({
            message: `Device not found`,
            duration: 2000
          });
          toast.present();
        }
      })
    if (data_hierarchy['levels']) {
      console.log(data_hierarchy['levels'],1);
      this.loading.dismiss();
      this.navCtrl.push(OfficedevicesPage, data_hierarchy['levels'])


    } else if (data_hierarchy['devices']) {
      console.log(data_hierarchy['devices'],2);

      this.loading.dismiss();

      this.navCtrl.push(DevicesPage, data_hierarchy['devices'])
    }
    else {
      console.log(data_hierarchy,3);
      this.loading.dismiss();
    
      this.navCtrl.push(MainPage, data_hierarchy)
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OfficedevicesPage');
  }
  ionViewWillLeave() {
    /*    var even = JSON.parse(localStorage.getItem('even'));
       localStorage.removeItem(localStorage.key(even.id)) */

  }
  /*  AddDevice() {
     const modal = this.modalCtrl.create(DrcodereaderPage);
     modal.present();
   } */

  go() {
    const modal = this.modalCtrl.create(AddDevicesPage);
    modal.present();
    // this.navCtrl.push(IframePage)
  }
  wifi() {
    /*     this.getSsidName();
     */
  }
  AddLevel() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
    localStorage.setItem("levelId._id", this.levelId._id)
    const modal = this.modalCtrl.create(AddlevelsPage);
    modal.present();
  }
}
