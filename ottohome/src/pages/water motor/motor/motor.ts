import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { WaterChatPage } from '../water-chat/water-chat';



@Component({
  selector: 'page-motor',
  templateUrl: 'motor.html',
})
export class MotorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MotorPage');
  }
  showCharts(){
    this.navCtrl.push(WaterChatPage)
  }
}
