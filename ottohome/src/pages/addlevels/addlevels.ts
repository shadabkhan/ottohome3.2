import { Component } from '@angular/core';
import {  NavController, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
// import { HTTP } from '@ionic-native/http';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-addlevels',
  templateUrl: 'addlevels.html',
})
export class AddlevelsPage {
  name: any;
  org_type: any;
  levelId: any;
  apiAddUrlorgLevel = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/levels';
  level: any;
  checkError: any;
  loading: any;
  _id: any;
  SubCategoryId: any;

  constructor(public loadingCtrl: LoadingController,public toastCtrl: ToastController,private http: Http,public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController) {
    this.levelId = this.navParams.data
    console.log(this.levelId)
   this.levelId= localStorage.getItem("levelId")
    }
    Categary() {
    
      console.log(this.SubCategoryId);
  
    }
  submit() {
    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    this.loading = this.loadingCtrl.create({
      content: 'Please wait  ...',
      enableBackdropDismiss: false
    });
    this.loading.present();
/*     var data = JSON.parse(postParams.token)
    console.log(JSON.parse(data.data))
    var datadata = JSON.parse(data.data)
    console.log("here is token", datadata.token)
    let headers = {
      'Authorization': 'Bearer ' + datadata.token
    }; */
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    const org = {
      _id: this._id,
      name: this.name,
      
      type: this.SubCategoryId,
      levelId: this.levelId,

      // imgPath:this.imgPaths

    }
    this.http.post(this.apiAddUrlorgLevel,org, options)
  .subscribe(data => {
    this.loading.dismiss();
    console.log(data);
    console.log(data); // data received by server
    this.navCtrl.push(HomePage)
    console.log(data);

  }, (error) => {
    console.log(error)
    console.log(error)
    this.loading.dismiss();

    console.log(error.status, 1);

    if( error.status == 0){
      let toast = this.toastCtrl.create({
        message: `Ehternet not connected`,
        duration: 2000
      });
      toast.present();
    }
   else if ( error.status == 409) {
      let toast = this.toastCtrl.create({
        message: `Level already added`,
        duration: 2000
      });
      toast.present();
    }
   /*  else if ( error.status == 401) {
      let toast = this.toastCtrl.create({
        message: `Invalid password`,
        duration: 2000
      });
      toast.present();
    }  else if ( error.status == 404) {
      let toast = this.toastCtrl.create({
        message: `user not found`,
        duration: 2000
      });
      toast.present();
    }  else if ( error.status == 400) {
      let toast = this.toastCtrl.create({
        message: `Only valid email will be chosen`,
        duration: 2000
      });
      toast.present();
    }  */
    else {
      // console.log(this.checkError.data[0].message);

      let toast = this.toastCtrl.create({
        message: `Some thing went rong`,
        duration: 2000
      });
      toast.present();
    }
  })
    // var user = JSON.parse(localStorage.getItem('user'));
    // let postParams = {
    //   // "Id": user.id,
    //   "token": user.token
    // }
    // let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    // let options = new RequestOptions({ headers: headers });
    // return new Promise((resolve, reject) => {
    //   this.http.post(this.apiAddUrlorgLevel,org, options)
    //     .subscribe(data => {
    //       data.json()
    //       this.level = data.json()
    //       console.log(this.level)
    //     }, (err) => {
    //       reject(err);
    //       console.log(err)
    //     });
    // });

  }
  dismiss() {
    this.navCtrl.push(HomePage)
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddlevelsPage');
  }

}
