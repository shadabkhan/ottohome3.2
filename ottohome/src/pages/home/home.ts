import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, MenuController, ToastController, ModalController, LoadingController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { Paho } from 'ng2-mqtt/mqttws31';
import { LoginPage } from '../login/login';
import { TabPage } from '../tab/tab';
import { MainPage } from '../main/main';

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
 import { MenuPage } from '../menu/menu';
import { OrganizationPage } from '../organization/organization';

import { Subscription } from 'rxjs/Subscription';
import { AddorgPage } from '../addorg/addorg';
import { MqttconnectionProvider } from '../../providers/mqttconnection/mqttconnection';
import { Events } from 'ionic-angular';
 import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { LogoutPage } from '../logout/logout';
import { AddlevelsPage } from '../addlevels/addlevels';
import { AdddeviceqrcodePage } from '../adddeviceqrcode/adddeviceqrcode';
import { AddOrgLevelPage } from '../add-org-level/add-org-level';
declare var google;
declare var Chart;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  connected: Subscription;
  disconnected: Subscription;
  hierarchy = 'https://linked-things-orgs.eu-gb.mybluemix.net/api/v1/organizations/';
  organization = 'https://linked-things-orgs.eu-gb.mybluemix.net/api/v1/users/me/organizations';
  levelHierarchy ='http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/users/me';
  apiAddUrlorgLevel = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/levels';
  data_hierarchy;
  payload: any;
  organiz;
  client: Paho.MQTT.Client;
  level: any;
  id = this.guid();
  sec: any;
  myLevels: any;
  subsmessage: any[] = [];
  org: any;
  password: any;
  _id: any;
  number: any;
  // tes :any;
  rootPage: HomePage;
  pages: { title: string; component: any; }[];
  constructor(private zone: NgZone, public events:Events,public toastCtrl: ToastController, private http: Http, public loadingCtrl: LoadingController, public MQTTSERVICE: MqttconnectionProvider, private nativePageTransitions: NativePageTransitions, public navCtrl: NavController, public menu: MenuController, private network: Network, private toast: ToastController, public modalCtrl: ModalController) //private http: HTTP
  {
   
  /*   this.events.subscribe('updateScreen', () => {
      this.zone.run(() => {
        console.log('force update the screen');
      });
    }); */
    this.menu.enable(true);
    this.network.onConnect().subscribe(data => {
      console.log(data)
      // this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
   
    this.network.onDisconnect().subscribe(data => {
      console.log(data)
      // this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
    // ======================================================================= Calling unique id start ================================================================================

    this.id;

    // ======================================================================= Calling unique id End ================================================================================

    // =======================================================================Calling Connection with broker and subscribe function  start ================================================================================


    // =======================================================================Calling Connection with broker and subscribe function  End ================================================================================
    // console.log(this.tes)
  }
  Addorganiziation() {
    const modal = this.modalCtrl.create(AddorgPage);
    modal.present();
  }
  presentModal() {

  }
  sendEsp(){
    const user = {
      _id: this._id,
      number: this.number,
  
      // imgPath:this.imgPaths
    }
    console.log(user)
    // var host = "*"
    var win = document.getElementsByTagName('iframe')[0];
    win.contentWindow.postMessage({ user }, "*")
  }
  ngOnInit() {
    this.events.subscribe('user:loggedin', () => {
      this.pages = [
        { title: 'Addorg', component: AddorgPage },
        { title: 'Esp', component: TabPage },
        { title: 'Logout', component: LogoutPage },

      ];
    });

    this.events.subscribe('user:loggedout', () => {
      this.pages = [
        { title: 'AddLevel', component: AddlevelsPage },
        { title: 'Add device', component: AdddeviceqrcodePage },
        { title: 'Logout', component: LogoutPage },

      ];
    });
    this.events.subscribe('user:addorglevel', () => {
      this.pages = [
        { title: 'AddLevel', component: AddOrgLevelPage },
        { title: 'Add device', component: AdddeviceqrcodePage },
        { title: 'Logout', component: LogoutPage },

      ];
    });
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    loading.present();

    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.levelHierarchy, options)
      
    .subscribe(data => {
      data.json()
      this.level = data.json()
      loading.dismiss();
      this.level = this.level['levels']
      console.log(this.level)
    }, (err) => {
      loading.dismiss();
      console.log(err);
    });
    // var user = localStorage.getItem('user');
    // let postParams = {
    //   // "Id": user.id,
    //   "token": user
    // }
    // var data = JSON.parse(postParams.token)
    // console.log(JSON.parse(data.data))
    // var datadata = JSON.parse(data.data)


    /*   let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
      let options = new RequestOptions({ headers: headers }); */
    // return new Promise((resolve, reject) => {
    //   this.http.get(this.organization, {},{})

    //     .then(data => {
    //      data
    //       // data.json()
    //      data
    //       this.level = data
    //       // loading.dismiss();
    //       console.log(this.level)
    //     }, (err) => {
    //       console.log(err)
    //     });
    // });
    // console.log("here is token", datadata.token)
    // let headers = {
    //   'Authorization': 'Bearer ' + datadata.token
    // };
    // this.http.get(this.levelHierarchy, {}, headers)
    //   .then(data => {
    //     loading.dismiss();
    //     console.log(data.status);
    //     console.log(JSON.parse(data.data));
    //     this.level = JSON.parse(data.data) // data received by server
    //     console.log(data.headers);

    //   })
    //   .catch(error => {
    //     loading.dismiss();
    //     console.log(error.status,0);
    //     if( error.status == 0){
    //       let toast = this.toastCtrl.create({
    //         message: `Ehternet not connected`,
    //         duration: 2000
    //       });
    //       toast.present();
    //     }
    //    else {
    //     let toast = this.toastCtrl.create({
    //       message: error.error.message,
    //       duration: 2000
    //     });
    //     toast.present()
    //     console.log(error.error,1,error.error.message,2); // error message as string
       
    //     console.log(error.headers,3);
    //   }
    //   });
    
  }
  showsubLevel(level) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    loading.present();
    console.log(level.levelId,'id')
    localStorage.setItem("org_id", level.levelId)
    this.organiz = level.levelId
  /*   var user = localStorage.getItem('user');
    let postParams = {
      // "Id": user.id,
      "token": user
    }
    var data = JSON.parse(postParams.token)
    console.log(JSON.parse(data.data))
    var datadata = JSON.parse(data.data)
    console.log("here is token", datadata.token)
    let headers = {
      'Authorization': 'Bearer ' + datadata.token
    };
 */
var user = JSON.parse(localStorage.getItem('user'));
let postParams = {
  // "Id": user.id,
  "token": user.token
}
let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
let options = new RequestOptions({ headers: headers });
    this.http.get(this.apiAddUrlorgLevel+"/"+ this.organiz + "/hierarchy",options)
      .subscribe(data => {
        console.log(data)
        // console.log(data.status);
        // console.log(JSON.parse(data.data));
        this.myLevels = data // data received by server
        // console.log(data.headers);
        /*  console.log(JSON.stringify(data))
         
         JSON.stringify(data)
         this.myLevels = JSON.stringify(data)
         console.log(this.myLevels)
       */

        loading.dismiss();
        /*  let options: NativeTransitionOptions = {
           direction: 'left',
           duration: 400,
           slowdownfactor: -1,
           iosdelay: 50
         };
 
         this.nativePageTransitions.slide(options);
 
         this.navCtrl.push(OrganizationPage, this.myLevels, { animate: true, animation: 'transition', duration: 500, direction: 'forward' });
  */
        this.navCtrl.push(OrganizationPage, this.myLevels)
      },(error) => {
        loading.dismiss();
        console.log(error.status,0);
        if( error.status == 0){
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
       else {
        let toast = this.toastCtrl.create({
          message: error.error.message,
          duration: 2000
        });
        toast.present()
        console.log(error.error,1,error.error.message,2); // error message as string
       
        console.log(error.headers,3);
      }
      });

  }

  // ======================================================================= Connection with broker and subscribe function  start ================================================================================



  ionViewWillLeave() {
    // this.client.unsubscribe('iot-2/type/Server/id/+/evt/Events/fmt/+', '');

  }

  onConnected() {
    console.log("Connected");
    this.client.subscribe("123456", "0");
    // this.client.subscribe("123456");
    this.sendMessage('HelloWorld');
  }

  sendMessage(message: string) {
    let packet = new Paho.MQTT.Message(message);
    packet.destinationName = "123456";
    this.client.send(packet);
  }
  // ======================================================================= Connection with broker and subscribe function  End ================================================================================



  // ======================================================================= generate unique id function  start ================================================================================


  guid() {
    let date = new Date();

    let str = date.getFullYear() + '' + date.getMonth() + '' + date.getDate() + '' + date.getHours() + '' + date.getMinutes() + '' + date.getSeconds();
    console.log(str)
    return str;
  }

  // ======================================================================= generate unique id function  End================================================================================

  // onMessage() {

  // }

  // onConnectionLost() {

  // }

  // ======================================================================= loguot function start================================================================================
  logout() {
    localStorage.clear();
    let options: NativeTransitionOptions = {
      direction: 'left',
      duration: 400,
      slowdownfactor: -1,
      iosdelay: 50
    };

    this.nativePageTransitions.slide(options);
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, animation: 'transition', duration: 500, direction: 'forward' });
  }


  // ======================================================================= loguot function End================================================================================

  // =======================================================================Creating object for grid view Start================================================================================

  tes = [
    {
      name: '4 devices',
      icon: 'ios-home-outline',
      handler: () => {
        console.log('Destructive clicked');
      }
    }, {
      name: 'Light',
      icon: 'ios-bulb-outline',
    }, {
      name: 'Fan',
      icon: 'ios-bookmark-outline',
    },
    {
      name: '4 devices',
      icon: 'ios-home-outline',
      handler: () => {
        console.log('Destructive clicked');
      }
    }, {
      name: 'Light',
      icon: 'ios-bulb-outline',
    }, {
      name: 'Fan',
      icon: 'ios-bookmark-outline',
    }

  ]
  // =======================================================================Creating object for grid view End================================================================================
  ionViewDidEnter() {
    this.network.onConnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

    this.network.onDisconnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
  }
  displayNetworkUpdate(connectionState: string) {
    let networkType = this.network.type;
   /*  this.toast.create({
      message: `You are now ${connectionState} via ${networkType}`,
      duration: 3000
    }).present(); */
  }
  doRefresh(refresher) {
    this.ngOnInit()
    console.log('Begin async operation', refresher);
    this.events.publish('updateScreen');
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  ionViewDidLoad() {
   
    this.events.publish('user:loggedin');
    // this.navCtrl.setRoot(DashboardPage);
this.org =true
localStorage.setItem("orghideshow",this.org)
  }
  ionViewCanLeave() {
    this.events.publish('user:addorglevel');
    this.org =false
    localStorage.setItem("orghideshow",this.org)
      }
    /*   ionViewWillLeave () {
        console.log( 'view will leave' );
      } */
    
      ionViewDidLeave () {
        console.log( 'view did leave' );
      }
}
