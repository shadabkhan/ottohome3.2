import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
// import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as moment from '../../../node_modules/moment';
import { MqttconnectionProvider } from '../../providers/mqttconnection/mqttconnection';
import { Subscription } from 'rxjs/Subscription';
// var IBMIoTF = require('ibmiotf');
import { HTTP } from '@ionic-native/http';

import * as ibmiotf from 'IBMIoTF'
// declare var ibmiotf:any;
declare var google;
declare var Chart;
declare var Morris;
 

@Component({
  selector: 'page-appliance',
  templateUrl: 'appliance.html',
})
export class AppliancePage {
  payloadData: any;
  deviceCompareEvents: any;
  todayYear: string;
  todaymonth: string;
  todaydate: string;
  todayHr: string;
  todayMn: string;
  todayHour: number;
  todayHourAdd5: number;
  todayMnsec: string;
  donut: any;
  motorStatusChart: any;
  motorStatusCreated: any;
  motorStatusValue: any;
  motordate;
  motortoday: string;
  jsmotortodaydate:Array<any> = [];
  messageEvent: string;
  timeWater: Array<any> = [];
  time: Array<any> = [];
  lateChipColor: any;
  rettimeWater:Array<any> = [];
  hubId(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  check: any;
  data_hierarchy: any;
  _id: any;
  created: any;
  level: any;
  levelId: any;
  createdBy: any;
  levelsLimit: any;
  name: any;
  schema: any;
  type: any;
  updated: any;
  _rev: any;
  value: number;
  devices = 'http://linked-things-events.eu-gb.mybluemix.net/api/v1/devices/';
  events: any;
  deviceId: any;
  charts: any;
  ttdtdt: any;
  temp_max: any;
  jsdate: Array<any> = [];
  ionicdate;
  today;
  todays: string;
  emptyspace: number;
  char: any;
  idHub: string;
  id = this.guid();
  connected: Subscription;
  disconnected: Subscription;
 
  payload: any;
  single: Array<any> = [];
  public buttonClicked: boolean;
 
  constructor( public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, private http: HTTP, public mqttservice: MqttconnectionProvider) {
    this.check = this.navParams.data
    console.log(this.check);
    this._id = this.navParams.data['_id'];
    this.deviceId = this.navParams.data['deviceId'];
    this.created = this.navParams.data['created'];
    this.createdBy = this.navParams.data['createdBy'];
    this.level = this.navParams.data['level'];
    this.levelId = this.navParams.data['levelId'];
    this.levelsLimit = this.navParams.data['levelsLimit'];
    this.name = this.navParams.data['name'];
    this.schema = this.navParams.data['schema'];
    this.type = this.navParams.data['type'];
    this.updated = this.navParams.data['updated'];
    this._rev = this.navParams.data['_rev'];
    this.value = this.navParams.data['value'];
    this.hubId = this.navParams.data['hubId'];
    this.today = new Date().toISOString().split(this.created)[0];
      console.log(this.today)
    localStorage.setItem('typedevice', this.type)
    console.log(this.hubId)
    console.log(this.type)
    console.log(this.value)
    console.log(this._id)
    this.showsubLevel();
  
    this.emptyspace = 100 - this.value
    console.log(this.emptyspace)
    console.log(this.type)
    this.todayYear= new Date().toISOString().split(this.created)[0].substring(0, 4);
    this.todaymonth = new Date().toISOString().split(this.created)[0].substring(5, 7);
    this.todaydate = new Date().toISOString().split(this.created)[0].substring(8, 10);
    this.todayHr = new Date().toISOString().split(this.created)[0].substring(11, 13);
    this.todayMn = new Date().toISOString().split(this.created)[0].substring(14, 16);
    this.todayMnsec = new Date().toISOString().split(this.created)[0].substring(17, 19);
    console.log(this.todayYear)
    console.log(this.todaymonth)
    console.log(this.todaydate)
    console.log(this.todayMn)
    console.log(this.todayMnsec)
  this.todayHour = parseInt(this.todayHr)
    
    this.todayHourAdd5 = this.todayHour
    console.log(this.todayHourAdd5)
    console.log(this.todayHourAdd5)
    // this.events = JSON.parse(localStorage.getItem('DeviceEvent'))
    console.log(this.events)
    var user = localStorage.getItem('user');
    let postParams = {
      // "Id": user.id,
      "token": user
    }
    var data = JSON.parse(postParams.token)
    console.log(JSON.parse(data.data))
    var datadata = JSON.parse(data.data)
    console.log("here is token", datadata.token)
    let headers = {
      'Authorization': 'Bearer ' + datadata.token
    };
   
    this.http.get(this.devices + this._id + "/events?type=WaterTank_Status",{},headers)
      .then(data => {
        JSON.parse(data.data)
        this.charts =JSON.parse(data.data)
        this.temp_max = this.charts.map(res => res.value)
        this.created = this.charts.map(res => res.created)
        for(let eachStatusStatus of this.created){
          
          // console.log("year",this.Year);
let hour = eachStatusStatus.toString().substring(8,10);
// console.log("mnt",month);
let mnt = eachStatusStatus.toString().substring(10,12);
// console.log("day",day);
let sec = eachStatusStatus.toString().substring(12,14);
let jointTimeWater = hour+':'+mnt+':'+sec;      
console.log("final",jointTimeWater);
   this.timeWater.push(jointTimeWater)
}
this.rettimeWater = new Array;
for(var i = this.timeWater.length-1; i >= 0; i--) {
  this.rettimeWater.push(this.timeWater[i]);
}
console.log(this.rettimeWater)
console.log("yeartimeWater",this.timeWater);
        console.log(this.charts)

      }).catch( (error) => {
        console.log(error)
        console.log(error)
       
   
        console.log(error.status, 1);
    
        if( error.status == 0){
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
      /*  else if ( error.status == 409) {
          let toast = this.toastCtrl.create({
            message: `email is has already been used`,
            duration: 2000
          });
          toast.present();
        }
        else if ( error.status == 401) {
          let toast = this.toastCtrl.create({
            message: `Invalid password`,
            duration: 2000
          });
          toast.present();
        }  else if ( error.status == 404) {
          let toast = this.toastCtrl.create({
            message: `user not found`,
            duration: 2000
          });
          toast.present();
        }  else if ( error.status == 400) {
          let toast = this.toastCtrl.create({
            message: `Only valid email will be chosen`,
            duration: 2000
          });
          toast.present();
        }  */
        else {
          // console.log(this.checkError.data[0].message);
          this.messageEvent = `Event not fired`
          let toast = this.toastCtrl.create({
            message: `Event not fired`,
            duration: 2000
          });
          toast.present();
        }
      })
      this.http.get(this.devices + this._id + "/events?type=Motor_Status",{},headers)
      .then(data => {
        JSON.parse(data.data)
        this.motorStatusChart = JSON.parse(data.data)
        this.motorStatusValue = this.motorStatusChart.map(data => data.value)
        this.motorStatusCreated = this.motorStatusChart.map(data =>data.created)
      console.log(this.motorStatusCreated)
         /*  console.log(this.motorStatusValue)
        console.log(this.motorStatusChart) */
        console.log(this.motorStatusChart)
        console.log(this.motorStatusValue)
        console.log(this.motorStatusValue[0])
        if(this.motorStatusValue[0] === 1 ){
          this.lateChipColor='rgb(204, 0, 51)';
          
        }
        else if(this.motorStatusValue[0] === 0 ) {
          this.lateChipColor='#343434';
        }
        for(let eachMotorStatus of this.motorStatusCreated){

          // console.log("year",this.Year);
let hour = eachMotorStatus.toString().substring(8,10);
// console.log("mnt",month);
let mnt = eachMotorStatus.toString().substring(10,12);
// console.log("day",day);
let sec = eachMotorStatus.toString().substring(12,14);
let jointTime = hour+':'+mnt+':'+sec;      
console.log("final",jointTime);
   this.time.push(jointTime)

}
console.log("year",this.time);

  let ret = new Array;
  for(var i = this.time.length-1; i >= 0; i--) {
      ret.push(this.time[i]);
  }
  console.log(ret)



       new Chart(document.getElementById("line-chart-motor"), {
          type: 'bar',
          data: {
            labels: ret,
            datasets: [
              {
                data: this.motorStatusValue,
              
                borderColor: "#CC0033",
                fill: true,
                hoverBackgroundColor: "#000000",
                borderWidth: 1
              }
            ]
          },
          options: {
           responsive: true,
           legend: {
             display: false,
           },
           hover: {
               mode: 'label'
           },
           scales: {
            /*    xAxes: [{
                  
                       scaleLabel: {
                           display: false,
                          
                       }
                   }], */
               yAxes: [{
                       display: true,
                       ticks: {
                           beginAtZero: true,
                           steps: 1,
                           stepValue: 5,
                           max: 1,
                           maxTicksLimit :1
                       }
                   }]
           },
           title: {
             display: true,
             text: 'MOTOR EVENT'
         }
       }
        });  
       
        Chart.defaults.global.elements.line.fill = true;
      }).catch( (error) => {
        console.log(error)
        console.log(error)
       
   
        console.log(error.status, 1);
    
        if( error.status == 0){
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
      /*  else if ( error.status == 409) {
          let toast = this.toastCtrl.create({
            message: `email is has already been used`,
            duration: 2000
          });
          toast.present();
        }
        else if ( error.status == 401) {
          let toast = this.toastCtrl.create({
            message: `Invalid password`,
            duration: 2000
          });
          toast.present();
        }  else if ( error.status == 404) {
          let toast = this.toastCtrl.create({
            message: `user not found`,
            duration: 2000
          });
          toast.present();
        }  else if ( error.status == 400) {
          let toast = this.toastCtrl.create({
            message: `Only valid email will be chosen`,
            duration: 2000
          });
          toast.present();
        }  */
        else {
          // console.log(this.checkError.data[0].message);
  
          let toast = this.toastCtrl.create({
            message: `Event not fired`,
            duration: 2000
          });
          toast.present();
        }
      })
    // stack issue solve
        // for (var i = 0; i < this.events.length; i++) {
        //   // console.log(this.events)
        //   var ismatch = false;
        //     // console.log(this.check)
    
        //     if (this.events[i].deviceId ==  this._id) {
        //       ismatch = true;
        //       // console.log(this.check)
    
        //       // this.deviceCompareEvents.push(this.events[i])
        //       console.log(this.events[i].value)
        //       this.value =this.events[i].value
        //       // localStorage.setItem("even", JSON.stringify(this.deviceCompareEvents))
        //       break;
        //       // console.log(this.newArray)
    
        //   }
    
        // }
  this.events = JSON.parse(localStorage.getItem('DeviceEvent'))
    console.log(this.events,'events')
    // stack issue solve
        for (var i = 0; i < this.events.length; i++) {
          // console.log(this.events)
          var ismatch = false;
            // console.log(this.check)
    
            if (this.events[i].deviceId ==  this._id) {
              ismatch = true;
              // console.log(this.check)
    
              // this.deviceCompareEvents.push(this.events[i])
              console.log(this.events[i].value,'value')
              this.value =this.events[i].value
              // localStorage.setItem("even", JSON.stringify(this.deviceCompareEvents))
              break;
              // console.log(this.newArray)
    
          }
    
        }
    var appClientConfig = {
      "org": "2uty47",
      "id": this.id,
      "auth-key": "a-2uty47-gjfzebtgtf",
      "auth-token": "TDrd8kQthdW5ypiZHo"
    };
    var appClient = new ibmiotf.IotfApplication(appClientConfig);

    appClient.connect();

    appClient.on("connect", function (connect) {
      console.log("connect : " + connect);
      /*  appClient.subscribeToDeviceEvents('Water');
       appClient.subscribeToDeviceEvents('Hub'); */
      /*  appClient.subscribeToDeviceEvents('Power'); */
      console.log(localStorage.getItem('typedevice'))

      appClient.subscribeToDeviceEvents('Server');


    });
    appClient.on("deviceEvent", (deviceType, deviceId, eventType, format, payload) => {
      console.log(eventType);
   
      console.log(this.deviceId)
      this.payloadData = JSON.parse(payload)
      console.log(this.payloadData);
      console.log(deviceId);
      if (this._id ==  this.payloadData.deviceId) {
        console.log(deviceId);
        console.log(this.payloadData)
        this.value = this.payloadData.value
        console.log(this.value);
        this.emptyspace = 100 - this.value
        this.todayYear= new Date().toISOString().split(this.created)[0].substring(0, 4);
        this.todaymonth = new Date().toISOString().split(this.created)[0].substring(5, 7);
        this.todaydate = new Date().toISOString().split(this.created)[0].substring(8, 10);
        this.todayHr = new Date().toISOString().split(this.created)[0].substring(11, 13);
        this.todayMn = new Date().toISOString().split(this.created)[0].substring(14, 16);
        this.todayMnsec = new Date().toISOString().split(this.created)[0].substring(17, 19);
        console.log(this.todayYear)
        console.log(this.todaymonth)
        console.log(this.todaydate)
        console.log(this.todayMn)
        console.log(this.todayMnsec)
      this.todayHour = parseInt(this.todayHr)
        
        this.todayHourAdd5 = this.todayHour
        console.log(this.todayHourAdd5)
   
        this.donut.setData([
          { label: "Filled", value: this.value },
          { label: "Empty", value: this.emptyspace },
        ]);
      }
    });
    appClient.on("error", function (err) {
      console.log("Error : " + err);
    });
  }
  publishedEvent(){
 
    this.value= this.motorStatusValue[0]
    if(this.value === 1 ){
      this.lateChipColor='rgb(204, 0, 51)';
      
    }
    else if(this.value === 0 ) {
      this.lateChipColor='#343434';
    }
    console.log(this.value)
    console.log(this.buttonClicked = !this.buttonClicked )

    if(this.buttonClicked == false){
      this.value =  0
    }
    else if(this.buttonClicked == true){
      this.value  = 1
    }
    console.log(this.value)
    var appClientConfig = {
      "org": "2uty47",
      "id": this.id,
      "auth-key": "a-2uty47-gjfzebtgtf",
      "auth-token": "TDrd8kQthdW5ypiZHo"
    };
    var appClient = new ibmiotf.IotfApplication(appClientConfig);
  
    appClient.connect();
    
    appClient.on("connect", ()=> {
        //publishing event using the default quality of service
          var data = {
            deviceId :this._id,
          hubId :this.hubId,
          value : this.value,
          type : "Motor_Status",
          createdBy : localStorage.getItem('userEmail')
        }
        
        appClient.publishDeviceCommand("Water",this.hubId,"AppEvents","json",data);
        // appClient.UnsubscribeToDeviceEvents('Server');
        //publishing event using the user-defined quality of service
     /*    var myQosLevel=2
        deviceClient.publish("status","json",'{"d" : { "cpu" : 60, "mem" : 50 }}', myQosLevel); */
    });
   }
  guid() {
    let date = new Date();

    let str = date.getFullYear() + '' + date.getMonth() + '' + date.getDate() + '' + date.getHours() + '' + date.getMinutes() + '' + date.getSeconds();
    console.log(str)
    return str;
  }
  showsubLevel() {
    console.log(this._id)
    /* var user = JSON.parse(localStorage.getItem('user'));
    let postParams = {
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });

    let options = new RequestOptions({ headers: headers });

    this.http.get(this.devices + this.deviceId + "/events?type=WaterTank_Status", options)
      .subscribe(data => {
        data.json()
        this.charts = data.json()
        this.temp_max = this.charts.map(res => res.value)
        this.created = this.charts.map((res) => {
          res.created
        })
        console.log(this.charts)

      }, (err) => {
        return err
      }); */

  }

 /*  showmotorLevel() {
    var user = JSON.parse(localStorage.getItem('user'));
    let postParams = {
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });

    let options = new RequestOptions({ headers: headers });

    this.http.get(this.devices + this.deviceId + "/events?type=WaterMotor_Status", options)
      .subscribe(data => {
        data.json()
        this.char = data.json()
        // this.temp_max = this.charts.map(res => res.value)
        // this.created = this.charts.map((res) => {
        //   res.created
        // })
        console.log(this.char)

      }, (err) => {
        return err
      });

  } */
  getTimeStr() {
    const date = new Date()

    const formatedDate = date.toISOString().substring(0, 10);
  }

  ionViewDidLoad() {

    this.showChart3();

    console.log('ionViewDidLoad AppliancePage', this.jsdate);
    setTimeout(() => { this.showChart1(); }, 3000);
 
  }

  showChart3() {
    this.emptyspace = 100 - this.value
   
    this.donut =  Morris.Donut({
      element: 'donut-example',
      colors	:["#CC0033", "#D98880"],
      data: [
        { label: "FILLED", value: this.value },
        { label: "Empty", value: this.emptyspace },

      ],
      resize:true
    });  
    console.log(Morris.Donut)
  }

  showChart1() {
     for (var i = 0; i < this.charts.length; i++) {
      this.ttdtdt = this.charts[i].value
      this.created = this.charts[i].created
      console.log(this.created)
      this.ionicdate = new Date(this.created)
      console.log(this.ionicdate)

      this.today = new Date().toISOString().split(this.ionicdate)[0].substring(11, 19);
      console.log(this.today)
      this.jsdate.push(this.today)


      console.log(this.jsdate)
   
    } 
//     for(let eachStatusStatus of this.created){

//       // console.log("year",this.Year);
// let hour = eachStatusStatus.toString().substring(8,10);
// // console.log("mnt",month);
// let mnt = eachStatusStatus.toString().substring(10,12);
// // console.log("day",day);
// let sec = eachStatusStatus.toString().substring(12,14);
// let jointTimeWater = hour+':'+mnt+':'+sec;
// console.log("final",jointTimeWater);
// this.timeWater.push(jointTimeWater)
// }
    var data = new Chart(document.getElementById("line-chart"), {
      type: 'line',
      data: {
        labels:  this.rettimeWater,
        datasets: [
          {
            data: this.temp_max,
            label: "",
            borderColor: "#CC0033",
            fill: true,
            hoverBackgroundColor: "#000000",
            borderWidth: 1
          }
        ]
      },
      options: {
        responsive: true,
        legend: {
          display: false,
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                      
                    }
                }],
            yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }]
        },
        title: {
            display: true,
            text: 'WATER CONSUMPTION GRAPH'
        }
    }
    });
    var data = new Chart(document.getElementById("line-chart2"), {
      type: 'line',
      data: {
        labels: this.rettimeWater,
        datasets: [
          {
            data: this.temp_max,
            label: "",
            borderColor: "#CC0033",
            fill: true,
            hoverBackgroundColor: "#000000",
            borderWidth: 1
          }
        ]
      },
      options: {
        responsive: true,
        legend: {
          display: false,
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                    }
                }],
            yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }]
        },
        title: {
            display: true,
            text: 'WATER CONSUMPTION GRAPH'
        }
    }
    });
    var data = new Chart(document.getElementById("line-chart3"), {
      type: 'line',
      data: {
        labels: this.rettimeWater,
        datasets: [
          {
            data: this.temp_max,
            
            borderColor: "#CC0033",
            fill: true,
            hoverBackgroundColor: "#000000",
            borderWidth: 1
          }
        ]
      },
      options: {
        responsive: true,
        legend: {
          display: false,
        },
        hover: {
            mode: 'label'
        },
        scales: {
            xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                      
                    }
                }],
            yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        steps: 10,
                        stepValue: 5,
                        max: 100
                    }
                }]
        },
        title: {
            display: true,
            text: 'WATER CONSUMPTION GRAPH'
        }
    }
    });
    Chart.defaults.global.elements.line.fill = true;
    console.log(this.ttdtdt)
    console.log(data.data.datasets[0].data)
  }

}
