import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the MergePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'merge',
})
export class MergePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(arr1, arr2) {
    var arr = [];
    arr1.forEach((elt, i) => {
      arr.push({ state: elt, name: arr2[i] });
    });
  }
}
