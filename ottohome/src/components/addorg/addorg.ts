import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { AddorgPage } from '../../pages/addorg/addorg';

/**
 * Generated class for the AddorgComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'addorg',
  templateUrl: 'addorg.html'
})
export class AddorgComponent {

  text: string;

  constructor(public modalCtrl: ModalController) {
    this.Addorganiziation()
  }
  Addorganiziation() {
    const modal = this.modalCtrl.create(AddorgPage);
    modal.present();
  }
}
