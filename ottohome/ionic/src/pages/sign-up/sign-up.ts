import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { HomePage } from '../home/home';

/**
 * Generated class for the SignUpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  _id: any;
  password: any;
  data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public AuthenticateProvider: AuthenticationProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }
  submit() {
    const user = {
      _id: this._id,
      password: this.password,

      // imgPath:this.imgPaths

    }

    this.AuthenticateProvider.signup(user).then(data => {
      data
      console.log(data)
      localStorage.setItem('user', JSON.stringify(data));
      // let options: NativeTransitionOptions = {
      //   direction: 'left',
      //   duration: 400,
      //   slowdownfactor: -1,
      //   iosdelay: 50
      // };

      // this.nativePageTransitions.slide(options);
      // this.navCtrl.setRoot(SignPage,{},{animate:true,animation:'transition',duration:500,direction:'forward'});
      this.navCtrl.setRoot(HomePage);



      // .then(data => {
      // data;
      //     console.log(data);
      //     this.data
      // })


    }).then(data => {
      // let loader = this.loadingCtrl.create({
      //       content: "Uploading..."
      //     });
      // loader.present();
      data;
      console.log(data);
      // loading.dismiss();

      this.data
      // let loading = this.loadingCtrl.create();
      // loading.present();
      // loader.dismiss();
      // this.navCtrl.setRoot(ProductcategoryPage);
      //     return false;
    }
      , (err) => {
        console.log(err);
        // loader.dismiss();
        // this.presentToast(err);
      });
  }
  // presentToast(msg) {
  //   let toast = this.toastCtrl.create({
  //     message: msg,
  //     duration: 3000,
  //     position: 'bottom'
  //   });

  //   toast.onDidDismiss(() => {
  //     console.log('Dismissed toast');
  //   });

  //   toast.present();
  // }
}
