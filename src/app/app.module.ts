import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Device } from '@ionic-native/device';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { SignUpPage } from '../pages/sign-up/sign-up';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { HTTP } from '@ionic-native/http';
import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { BlubComponent } from '../components/blub/blub';
import { MainPage } from '../pages/main/main';
import { TabPage } from '../pages/tab/tab';
import { MenuPage } from '../pages/menu/menu';
import { OrganizationPage } from '../pages/organization/organization';
import { DevicesPage } from '../pages/devices/devices';
import { OfficedevicesPage } from '../pages/officedevices/officedevices';
import { AppliancePage } from '../pages/appliance/appliance';
import { MergePipe } from '../pipes/merge/merge';
import { MotorPage } from '../pages/water motor/motor/motor';
import { WaterChatPage } from '../pages/water motor/water-chat/water-chat';
import { Network } from '@ionic-native/network';
import { DrcodereaderPage } from '../pages/qrcode/drcodereader/drcodereader';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Base64 } from '@ionic-native/base64';
import { Hotspot } from '@ionic-native/hotspot';
import { IframePage } from '../pages/iframe/iframe';
import { WaterPage } from '../pages/water/water';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AddorgPage } from '../pages/addorg/addorg';
import { AddlevelsPage } from '../pages/addlevels/addlevels';
import { AddDevicesPage } from '../pages/add-devices/add-devices';
import { MqttconnectionProvider } from '../providers/mqttconnection/mqttconnection';
import { EmailverifyPage } from '../pages/emailverify/emailverify';
import { AdddeviceqrcodePage } from '../pages/adddeviceqrcode/adddeviceqrcode';
import { LogoutPage } from '../pages/logout/logout';
import { AddOrgLevelPage } from '../pages/add-org-level/add-org-level';
import { GaugeModule } from 'angular-gauge';
import { EmailComposer } from '@ionic-native/email-composer';
import { ApiStarterPage } from '../pages/api-starter/api-starter';
 

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignUpPage,
    BlubComponent,
    MainPage,
    TabPage,
    MenuPage,
    OrganizationPage,
    DevicesPage,
    OfficedevicesPage,
    AppliancePage,
    MergePipe,
    MotorPage,
    WaterChatPage,
    DrcodereaderPage,
    IframePage,
    WaterPage,
    AddorgPage,
    AddlevelsPage,
    AddDevicesPage,
    EmailverifyPage,
    AdddeviceqrcodePage,
    LogoutPage,
    AddOrgLevelPage,
    ApiStarterPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    GaugeModule.forRoot()
 
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignUpPage,
    BlubComponent,
    MainPage,
    TabPage,
    MenuPage,
    OrganizationPage,
    DevicesPage,
    OfficedevicesPage,
    AppliancePage,
    MotorPage,
    WaterChatPage,
    DrcodereaderPage,
    IframePage,
    WaterPage,
    AddorgPage,
    AddlevelsPage,
    AddDevicesPage,
    EmailverifyPage,
    AdddeviceqrcodePage,
    LogoutPage,
    AddOrgLevelPage,
    ApiStarterPage
  ],
  providers: [
    InAppBrowser,
    Base64,
    Network,
    NativePageTransitions,
    HTTP,
    EmailComposer,
    Device,
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthenticationProvider,
    BarcodeScanner,
    Hotspot,
    MqttconnectionProvider
  ]
})
export class AppModule { }
