import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, Nav, ToastController, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { MainPage } from '../pages/main/main';
import { TabPage } from '../pages/tab/tab';
import { MenuPage } from '../pages/menu/menu';
import { MotorPage } from '../pages/water motor/motor/motor';
import { Network } from '@ionic-native/network';
import { DrcodereaderPage } from '../pages/qrcode/drcodereader/drcodereader';
import { AddorgPage } from '../pages/addorg/addorg';
import { AddlevelsPage } from '../pages/addlevels/addlevels';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Hotspot } from '@ionic-native/hotspot';
import { AddDevicesPage } from '../pages/add-devices/add-devices';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { Events } from 'ionic-angular';
import { AddorgComponent } from '../components/addorg/addorg';
import { AddlevelComponent } from '../components/addlevel/addlevel';
import { AdddevicesComponent } from '../components/adddevices/adddevices';
import { AdddeviceqrcodePage } from '../pages/adddeviceqrcode/adddeviceqrcode';
import { LogoutPage } from '../pages/logout/logout';
import { AddOrgLevelPage } from '../pages/add-org-level/add-org-level';
import { ApiStarterPage } from '../pages/api-starter/api-starter';
declare var WifiWizard;
declare var BMSClient;
declare var BMSPush;
declare var d3v3min
declare var liquidFillGauge
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  loggedIn: boolean;
  pages: { title: string; component: any; }[];
  levelId: any;
  id: any;
  pass: any;
  str: any;
  selectedProduct: any;
  orghideshow: any;
  constructor(events: Events, public authService: AuthenticationProvider, private toast: ToastController, private hotspot: Hotspot, private barcodeScanner: BarcodeScanner, public modalCtrl: ModalController, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,   private network: Network, public toastCtrl: ToastController) {

// initialize BMSCore SDK
 
// initialize BMSPush SDK
// var pushAppGUID = "Your Push service appGUID";
// var clientSecret = "Your Push service clientSecret";

// // Initialize for normal push notifications
// var options = {}
// // BMSPush.initialize(appGUID,clientSecret,options);
// BMSClient.initialize(pushAppGUID, clientSecret,options)
/* 
    var success = function(response) { console.log("Success: " + response); };
var failure = function(response) { console.log("Error: " + response); };
var options = {"userId": localStorage.getItem('userEmail')};
BMSPush.registerDevice(options, success, failure); */
    var user = JSON.parse(localStorage.getItem('user'));

    console.log('my cart: ', user);

    this.loggedIn = user;
    if (!this.loggedIn) {
      this.rootPage = LoginPage;
    }
    else if (this.loggedIn) {
      this.rootPage = ApiStarterPage;
    }

    this.network.onConnect().subscribe(data => {
      console.log(data)
    }, error => console.error(error));

    this.network.onDisconnect().subscribe(data => {
      console.log(data)
    }, error => console.error(error));
  
    /*    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
         console.log('network was disconnected :-(');
         this.netdis();
       });
   
       // stop disconnect watch
       disconnectSubscription.unsubscribe();
   
   
       let connectSubscription = this.network.onConnect().subscribe(() => {
         console.log('network connected!');
         // We just got a connection but we need to wait briefly
         // before we determine the connection type. Might need to wait.
         // prior to doing any api requests as well.
         setTimeout(() => {
           if (this.network.type === 'wifi') {
             console.log('we got a wifi connection, woohoo!');
             this.presentToast();
           }
         }, 3000);
       });
   
       // stop connect watch
       connectSubscription.unsubscribe(); */

    /*     this.net(); */

    events.subscribe('user:loggedin', () => {
      this.pages = [
        { title: 'Home', component: HomePage },
        { title: 'Addorg', component: AddorgPage },
        { title: 'Esp', component: TabPage },
        { title: 'Logout', component: LogoutPage },

      ];
    });

    events.subscribe('user:loggedout', () => {
      this.pages = [
        { title: 'Home', component: HomePage },
        { title: 'AddLevel', component: AddlevelsPage },
        { title: 'Add device', component: AdddeviceqrcodePage },
        { title: 'Logout', component: LogoutPage },

      ];
    });
    events.subscribe('user:addorglevel', () => {
      this.pages = [
        { title: 'Home', component: HomePage },
        { title: 'AddLevel', component: AddOrgLevelPage },
        { title: 'Add device', component: AdddeviceqrcodePage },
        { title: 'Logout', component: LogoutPage },

      ];
    });
    platform.ready().then(() => {
  

BMSClient.initialize(".eu-gb.bluemix.net");
var appGUID = "79da6d54-7660-4c9e-86df-8bf44ddebdde";
var clientSecret = "c4f999ab-683b-4e7e-a330-120770ab9649";
 var options = {};//"userId": "shammadahmed31@gmail.com"
BMSPush.initialize(appGUID,clientSecret,options);

var success = function(response) { 
  console.log("Success: " + response); 
  var token = JSON.parse(response)
  console.log("Success: " + token); 
};
var failure = function(response) { 
  console.log("Error: " + response); 
  var token = JSON.parse(response)
  console.log("Error: " + token); 
};
var options = {};
BMSPush.registerDevice(options, success, failure);

// Register device for push notification without UserId
 
// BMSPush.registerDevice(options, success, failure);

   
      WifiWizard.getCurrentSSID(ssidHandler, fail)

      function ssidHandler(s) {
        console.log("Current SSID" + s);
        JSON.stringify(localStorage.setItem("ssid",s))
        localStorage.setItem("ssid", JSON.parse(s))
      } function fail(e) {
        alert("Failed" + e);
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.


      statusBar.styleDefault();
      splashScreen.hide();
      // this.pushSetup();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  connectnet() {
    // watch network for a connection

  }
  presentToast() {
    const toast = this.toastCtrl.create({
      message: 'we got a wifi connection, woohoo!',
      duration: 3000
    });
    toast.present();
  }
  netdis() {
    const toast = this.toastCtrl.create({
      message: 'network was disconnected :-(',
      duration: 3000
    });
    toast.present();
  }
/*   pushSetup() {
    const options: PushOptions = {
      android: {
        senderID: '18576018400'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'false'
      }
    };

    const pushObject: PushObject = this.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

    pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));

    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  } */

  logout() {
    localStorage.clear();
    this.nav.setRoot(LoginPage)
  }
  Addorganiziation() {
    const modal = this.modalCtrl.create(AddorgPage);
    modal.present();
  }
  isAuthenticated() {
    this.orghideshow = localStorage.getItem('orghideshow')
    return this.orghideshow
  }

  go() {
    const modal = this.modalCtrl.create(AddDevicesPage);
    modal.present();
    // this.navCtrl.push(IframePage)
  }
  AddDevice() {
    // this.selectedProduct = {};
    this.barcodeScanner.scan().then((barcodeData) => {

      this.selectedProduct = barcodeData;

      console.log(this.selectedProduct.text.split(','))
      this.str = this.selectedProduct.text.split(',')

      var ssid = this.str[0].toString()
      var ssps = this.str[1].toString()
      this.id = atob(ssid)
      this.pass = atob(ssps)
      console.log(this.id);
      console.log(this.pass);

      const espwifi = {
        id: this.id,
        pass: this.pass
      }
      localStorage.setItem("espwifissid", this.id)
      localStorage.setItem("espwifipass", this.pass)

      this.hotspot.connectToWifiAuthEncrypt(this.id, this.pass, "OPEN", ["CCMP", "TKIP", "WEP40", "WEP104"]).then(data => {
        console.log('Connected to User');

        // display alert here
        alert(data)
    /*     let toast = this.toast.create({
          message: "change wifi ",
          duration: 2000
        });
        toast.present(); */
        this.go()
    
      }).catch(err => {

        let toast = this.toast.create({
          message: err,
          duration: 2000
        });
        toast.present();
     
        console.log(err)
      });
      // WifiWizard.removeNetwork(s);

      // WifiWizard.connectNetwork(this.id, this.pass, this.faile);

      console.log("password", this.id, this.pass);
      /*  this.hotspot.removeWifiNetwork(ssid).then( () => {
            this.hotspot.connectToWifi(this.id, this.pass) 
                .then((data) => {
                      console.log(".........hotspot..........",data);
                 }, (error) => {
                      console.log(".........hotspot..........",error);
                 });
       }, () => {
       }); */
      // WifiWizard.connectNetwork(this.id, this.pass,this.win,this.faile)
      // this.getCurrentSSID();
      // .then(data =>{console.log(data)}).catch(err => {console.log(err)});
      // this.hotspot.removeWifiNetwork(this.ssid).then(data =>{console.log(data)}).catch(err => {console.log(err)});
      // this.hotspot.connectToWifi(this.id, this.pass).then(data =>{console.log(data)}).catch(err => {console.log(err)});
      /*    WifiWizard.addNetwork(wifi.id,wifi.pass).then(data =>{
           console.log(data)
         }).catch(err => console.log(err)); */
      /*   this.hotspot.removeWifiNetwork(this.id).then( () => {
          this.hotspot.connectToWifi(this.id, this.pass) 
              .then((data) => {
                    console.log(data);
               }, (error) => {
                    console.log(error);
               });
     }, (error) => {
      console.log(error);
  }); */
      /*   this.hotspot.connectToWifi(this.id, this.pass).then( conn =>{
          console.log(conn)
        }).catch(err =>{
          console.log(err)
  
        }) */
      /* this.hotspot.addWifiNetwork(this.id,"Open", this.pass).then( conn =>{
        console.log(conn)
      }).catch(err =>{
        console.log(err)

      }) */

      // var ssid = str[1].toString()
      //   if(this.selectedProduct !== undefined) {
      //     this.productFound = true;
      //   } else {
      //     this.productFound = false;
      //     this.toast.show(`Product not found`, '5000', 'center').subscribe(
      //       toast => {


      //       }
      //     );
      //   }
      // }, (err) => {
      //   this.toast.show(err, '5000', 'center').subscribe(
      //     toast => {
      //       console.log(toast);
      //     }
      //   );
    }).catch(error => {
      alert(JSON.parse(error))
      console.log(error);
      console.log(error.error); // error message as string
      console.log(error.headers);

    });
  }
}

