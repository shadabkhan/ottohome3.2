import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { AddlevelsPage } from '../../pages/addlevels/addlevels';

/**
 * Generated class for the AddlevelComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'addlevel',
  templateUrl: 'addlevel.html'
})
export class AddlevelComponent {

  text: string;

  constructor(public modalCtrl: ModalController) {
  this.AddLevel();
  }
  AddLevel() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
    
    const modal = this.modalCtrl.create(AddlevelsPage);
    modal.present();
  }
}
