import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { WaterPage } from '../water/water';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Hotspot } from '@ionic-native/hotspot';
import { DevicesPage } from '../devices/devices';
import { DrcodereaderPage } from '../qrcode/drcodereader/drcodereader';
import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

@Component({
  selector: 'page-iframe',
  templateUrl: 'iframe.html',
})
export class IframePage {
  _id: string;
  orgid: string;
  visability: any;
  lenghtOh: any;
  widthOh: any;
  heightOH: any;
  visabili: any;
  lenghtUG: any;
  widthUG: any;
  heightUG: any;
  OHMotorselect: any;
  UGMotorselect: any;


  @ViewChild('iframe') iframe: ElementRef;  // sketchElement:ElementRef;
  credential;
  organization = 'http://192.168.4.1/'
  addHub = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/hubs'
  level: any;
  ssid: any;
  password: any;
  mes: any[] = [];
  mac_id: string;
  mac_Type: string;
  DeviecLevelId: string;
  pass: string;
  IdPass: any;
  loader: any;
  checkError: any;
  httpflag: boolean;
  toastctrl: any;
  constructor(private network: Network, private http: Http, public loadingCtrl: LoadingController, public viewCtrl: ViewController, private toast: ToastController, private hotspot: Hotspot, private iab: InAppBrowser, public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
    window.addEventListener("message", receiveMessage, false);
    function receiveMessage(event) {
      if (event.origin !== 'http://192.168.4.1') //check origin of message for security reasons
      {
        console.log('URL issues');
        return;
      }
      else {

        console.log('URL');
      }
    }

    var ssid = localStorage.getItem('ssid')
    let org_id = localStorage.getItem('org_id')
    this._id = ssid
    this.orgid = org_id
    var IdPass = localStorage.getItem('wifiIdPass')
    this.password = IdPass

    // this.password = this.IdPass.password
  }
 
  ionViewDidLoad() {
    var win = document.getElementsByTagName('iframe')[0].contentWindow;
    console.log(win);
    window.addEventListener('message', function (e) {
      var message = e.data;
      console.log(message.result)
      console.log(message.res)
      localStorage.setItem("mac_id", message.result)
      localStorage.setItem("mac_Type", message.res)
    });

    // this.presentModal();
  }
  getdata() {

  }
  wotorSubmit() {
    this.loader = this.loadingCtrl.create({
      content: `Please weight...`,
    });
    this.loader.present();
    let ssid = localStorage.getItem('ssid')

    const user = {
      _id: this._id,
      password: this.password,
      org_id: this.orgid,
      OH: this.visability,
      lenghtOh: this.lenghtOh,
      widthOh: this.widthOh,
      heightOH: this.heightOH,
      UG: this.visabili,
      lenghtUG: this.lenghtUG,
      widthUG: this.widthUG,
      heightUG: this.heightUG,
      OHMotorselect: this.OHMotorselect,
      UGMotorselect: this.UGMotorselect,

    }
    console.log(user)
    // var host = "*"
    var win = document.getElementsByTagName('iframe')[0];
    win.contentWindow.postMessage({ user }, "*")

    //  window.parent.postMessage({user }, "*")
    console.log('received a message!', user);
 

    this.network.onConnect().subscribe(data => {
      console.log(data)
      this.addHubdevices()
      this.loader.dismiss();
      /*   if(this.httpflag  == false ){
      
        this.addHubdevices()
        this.httpflag  = true
      } */
      // this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

  }
  addHubdevices() {
  
    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.mac_id = localStorage.getItem("mac_id")
    this.mac_Type = localStorage.getItem("mac_Type")
    this.DeviecLevelId = localStorage.getItem("levelId")
    if (this.mac_Type == 'Water') {
      var obj = {

        "_id": this.mac_id,
        "levelId": this.DeviecLevelId,
        "authToken": this.mac_id,
        "name": "Hub For" + this.mac_Type,
        "version": "0.0.0",
        "type": this.mac_Type

      }
      console.log(obj)
      console.log('obj', JSON.stringify(obj))
      this.http.post(this.addHub, obj, options)
        .subscribe(data => {
          this.loader.dismiss().then(() => this.navCtrl.setRoot(HomePage));
          console.log(data);
          console.log(data);
          console.log(data); // data received by server
          console.log(data);

        }, (error) => {

        /*   this.loader.dismiss();
          this.checkError = JSON.parse(error.error)
          this.checkError.message
          console.log(error.status);
          console.log(JSON.parse(error.error)); // error message as string
          console.log(error.headers); */
          
          this.navCtrl.push(HomePage)
        });

    }
  }
  presentModal() {
    const modal = this.modalCtrl.create(WaterPage);
    modal.present();
  }
  change() {
    this.visability = true;
  }
  datachanged(e: any) {
    console.log(e);
    this.visability = e.checked
    console.log(this.visability);
  }
  datachang(e: any) {
    console.log(e);
    this.visabili = e.checked
    console.log(this.visabili);

  }
  OHMotor(e) {
    console.log(e);
    this.OHMotorselect = e.checked
    console.log(this.OHMotorselect);

  }
  UGMotor(e) {
    console.log(e);
    this.UGMotorselect = e.checked
    console.log(this.UGMotorselect);

  }
  onChange(items) {
    console.log('onChange', items);
    let isChecked = false;
    for (let i = 0; i < items.length; i++) {
      if (items[i].checkBox == true) {
        isChecked = true;
        this.visability = true;
      }
    }
    if (isChecked == false) {
      this.visability = false;
    }
  }
  dismis() {
    this.viewCtrl.dismiss()
  }
}
