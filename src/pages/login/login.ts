import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, ToastController } from 'ionic-angular';
import { SignUpPage } from '../sign-up/sign-up';
import { HomePage } from '../home/home';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
 
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { ApiStarterPage } from '../api-starter/api-starter';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  data: any;
  _id: any;
  password: any;
  public type = 'password';
  public showPass = false;
  checkError: any;
  apiUrl = 'http://linked-things-orgs.eu-gb.mybluemix.net/api/v1/users/';
  loading: any;
  userObj: any;
  userLogin = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/users/signin'
  constructor(public http: HttpClient,public toastCtrl: ToastController, public loadingCtrl: LoadingController,
    public menuCtrl: MenuController
    , private nativePageTransitions: NativePageTransitions, public navCtrl: NavController, public navParams: NavParams, public AuthenticateProvider: AuthenticationProvider) {
    this.menuCtrl.enable(false, 'myMenu');
  }
  showPassword() {
    this.showPass = !this.showPass;

    if (this.showPass) {
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  signUp() {
    let options: NativeTransitionOptions = {
      direction: 'left',
      duration: 400,
      slowdownfactor: -1,
      iosdelay: 50
    };

    this.nativePageTransitions.slide(options);
    this.navCtrl.setRoot(SignUpPage, {}, { animate: true, animation: 'transition', duration: 500, direction: 'forward' });
  }

  login() {

    const user = {
      _id: this._id,
      password: this.password,

      // imgPath:this.imgPaths

    }
    if(!this.validateEmail(user._id)){
      let toast = this.toastCtrl.create({
        message: `Email not valid`,
        duration: 4000
      });
      toast.present();
     
    }else{
    this.loading = this.loadingCtrl.create({
      content: 'Please wait user login ...',
      enableBackdropDismiss: false
    });

    this.loading.present();
    let headers = {
      'Content-Type': 'application/json'
    };
    
    this.http.post(this.userLogin, user)
    .subscribe(data => {
      
      console.log(user._id)
      localStorage.setItem('userEmail', user._id);
      this.loading.dismiss();
      console.log(data['message']);
      // console.log(JSON.stringify(data));
      // this.data  =JSON.parse(JSON.stringify(data))
      console.log(data['statusCode'] );

      if(data['statusCode'] == 401){
        let toast = this.toastCtrl.create({
          message: data['message'],
          duration: 2000
        });
        toast.present();
      }else if(data['statusCode'] == 404){
        let toast = this.toastCtrl.create({
          message: data['message'],
          duration: 2000
        });
        toast.present();
      }
      
      
      
      else{
        JSON.stringify(data)
        console.log(JSON.stringify(data));
        this.userObj = data
        console.log(this.userObj );
        localStorage.setItem('user', JSON.stringify(this.userObj));
       
        this.navCtrl.setRoot(ApiStarterPage)
      }
     
     },(error) => {
      console.log(error)
      console.log(error)
      this.loading.dismiss();
 
      console.log(error.status, 1);
  
      if( error.status == 0){
        let toast = this.toastCtrl.create({
          message: `Ehternet not connected`,
          duration: 2000
        });
        toast.present();
      }
     else if ( error.status == 409) {
        let toast = this.toastCtrl.create({
          message: `email is has already been used`,
          duration: 2000
        });
        toast.present();
      }
      else if ( error.status == 401) {
        let toast = this.toastCtrl.create({
          message: `Invalid password`,
          duration: 2000
        });
        toast.present();
      }  else if ( error.status == 404) {
        let toast = this.toastCtrl.create({
          message: `user not found`,
          duration: 2000
        });
        toast.present();
      }  else if ( error.status == 400) {
        let toast = this.toastCtrl.create({
          message: `Only valid email will be chosen`,
          duration: 2000
        });
        toast.present();
      } 
      else if ( error.status == 500) {
        let toast = this.toastCtrl.create({
          message: `Internal Serve Error`,
          duration: 2000
        });
        toast.present();
      } 
      else {
        // console.log(this.checkError.data[0].message);

        let toast = this.toastCtrl.create({
          message: `Only valid email will be chosen`,
          duration: 2000
        });
        toast.present();
      }
    })
  }
  }
  validateEmail(email){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     return re.test(email);
 }
}
