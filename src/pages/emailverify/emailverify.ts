import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import * as launcher from '../../assets/js/start-app';
import { LoginPage } from '../login/login';
/**
 * Generated class for the EmailverifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 
@Component({
  selector: 'page-emailverify',
  templateUrl: 'emailverify.html',
})
export class EmailverifyPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EmailverifyPage');
  }
  mailto() {
    launcher.packageLaunch("com.google.android.gm");
  }
  login(){
    this.navCtrl.push(LoginPage)
  }
}
