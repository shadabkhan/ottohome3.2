import { Component, ViewChild } from '@angular/core';
import {  NavController, NavParams, Nav, ViewController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { MainPage } from '../main/main';
import { TabPage } from '../tab/tab';
import { LoginPage } from '../login/login';



@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
 


  constructor(public navCtrl: NavController, public viewCtrl: ViewController,public navParams: NavParams) {
  }
  ionViewDidLoad() {
 
  }
  logout() {
    localStorage.clear();
    this.navCtrl.setRoot(LoginPage)
  }
  close() {
    this.viewCtrl.dismiss();
  }
}
