import { Component } from '@angular/core';
import {  NavController, NavParams, ModalController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { TabPage } from '../tab/tab';
import { SignUpPage } from '../sign-up/sign-up';
import { MenuPage } from '../menu/menu';
import { AddlevelsPage } from '../addlevels/addlevels';
import { AddDevicesPage } from '../add-devices/add-devices';
import { ApiStarterPage } from '../api-starter/api-starter';



@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})
export class MainPage {
  HomePage = HomePage
  MainPage = TabPage
  /* 
  TabPage = TabPage
  LoginPage = TabPage */

  myIndex: number;
  sec: any;
  data_hierarchy: any;
  connected: any;
 
  _id: any;
  created: any;
  level: any;
  levelId: any;
  createdBy: any;
  levelsLimit: any;
  name: any;
  schema: any;
  type: any;
  updated: any;
  _rev: any;
  check: Array<any> = [];
  deviceCompareEvents: Array<any> = [];
  devices = 'https://linked-things-events.eu-gb.mybluemix.net/api/v1/levels/';
  events: any[];
  checkd: any[];
  newArray: any[];
  selectedProduct: any;
  pass: any;
  id: any;
  str: any;
  deviceId: any;
  value: any;
  hubId: any;
  constructor(public navCtrl: NavController,  public modalCtrl: ModalController,public navParams: NavParams) {
    console.log(this.navParams.data);
    this.check = this.navParams.data
    console.log(this.check);
    this._id = this.navParams.data['_id'];
    this.deviceId = this.navParams.data['deviceId'];
    this.created = this.navParams.data['created'];
    this.createdBy = this.navParams.data['createdBy'];
    this.level = this.navParams.data['level'];
    this.levelId = this.navParams.data['levelId'];
    this.levelsLimit = this.navParams.data['levelsLimit'];
    this.name = this.navParams.data['name'];
    this.schema = this.navParams.data['schema'];
    this.type = this.navParams.data['type'];
    this.updated = this.navParams.data['updated'];
    this._rev = this.navParams.data['_rev'];
    this.value = this.navParams.data['value'];
    this.hubId = this.navParams.data['hubId'];
    localStorage.setItem('typedevice', this.type)
    console.log(this.hubId)
    console.log(this.type)
    console.log(this.value)
    console.log(this._id,'levelid')

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }
  showsubLevel(data_hierarchy){
    console.log(data_hierarchy);
    this.sec =data_hierarchy
    this.navCtrl.push(TabPage,this.sec)
  }
  AddLevel() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
    const modal = this.modalCtrl.create(AddlevelsPage, this.levelId._id);
    modal.present();
  }
  AddDevice() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
    const modal = this.modalCtrl.create(AddDevicesPage, this.levelId._id);
    modal.present();
  }
  home(){
    this.navCtrl.push(ApiStarterPage)
  }
}
