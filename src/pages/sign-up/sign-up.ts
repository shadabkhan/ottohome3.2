import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, ToastController, Alert } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { EmailverifyPage } from '../emailverify/emailverify';
// import { HTTP } from '@ionic-native/http';
import { Http, Headers, RequestOptions, Response } from '@angular/http';


@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {
  _id: any;
  password: any;
  data: any;
  checkError: any;
  loading: any;
  apiUrl = 'http://linked-things-orgs.eu-gb.mybluemix.net/api/v1/users/';
  usersignup = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/users/'

  constructor(private http: Http,public toastCtrl: ToastController,private nativePageTransitions: NativePageTransitions,public loadingCtrl: LoadingController,public menuCtrl: MenuController,public navCtrl: NavController, public navParams: NavParams, public AuthenticateProvider: AuthenticationProvider ) {
    this.menuCtrl.enable(false, 'myMenu');

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpPage');
  }
  submit() {
 
    
    
    const user = {
      _id: this._id,
      password: this.password,

      // imgPath:this.imgPaths

    }
    if(!this.validateEmail(user._id)){
      let toast = this.toastCtrl.create({
        message: `Email not valid`,
        duration: 4000
      });
      toast.present();
     
    }else{
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    this.loading.present();

    
 /*    let headers = {
      'Content-Type': 'application/json'
    }; */
    this.http.post(this.usersignup + "signup", user)
    .subscribe(data => {
      this.loading.dismiss();
      console.log(data)
      this.navCtrl.push(EmailverifyPage)
    }, (error) => {
      console.log(error)
      console.log(error)
      this.loading.dismiss();
 
      console.log(error.status, 1);
  
      if( error.status == 0){
        let toast = this.toastCtrl.create({
          message: `Ehternet not connected`,
          duration: 2000
        });
        toast.present();
      }
     else if ( error.status == 409) {
        let toast = this.toastCtrl.create({
          message: `email is has already been used`,
          duration: 2000
        });
        toast.present();
      } else {
        // console.log(this.checkError.data[0].message);

        let toast = this.toastCtrl.create({
          message: `Only valid email will be chosen`,
          duration: 2000
        });
        toast.present();
      }
    })
  }
 /*    this.AuthenticateProvider.signup(user).then(data => {
      data;
      console.log(data);
      this.data
      this.navCtrl.setRoot(LoginPage)
    
      }).catch( (error) => {
        console.log(error)
        this.loading.dismiss();
        this.checkError = JSON.parse(error.error)
        this.checkError.message
        console.log(error.status, 1);
        let toast = this.toastCtrl.create({
          message: this.checkError.message,
          duration: 2000
        });
        toast.present();
        console.log(JSON.parse(error.error), 2); // error message as string
        console.log(error.headers, 3);
        if (this.checkError.message == "409 Conflict") {
          let toast = this.toastCtrl.create({
            message: `email is has already been used`,
            duration: 2000
          });
          toast.present();
        } else {
          console.log(this.checkError.data[0].message);
  
          let toast = this.toastCtrl.create({
            message: `Only valid email will be chosen`,
            duration: 2000
          });
          toast.present();
        }
      }) */
  }

  // presentToast(msg) {
  //   let toast = this.toastCtrl.create({
  //     message: msg,
  //     duration: 3000,
  //     position: 'bottom'
  //   });

  //   toast.onDidDismiss(() => {
  //     console.log('Dismissed toast');
  //   });

  //   toast.present();
  // }
  Newhere(){
    this.navCtrl.push(LoginPage)
  }
  validateEmail(email){
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     return re.test(email);
 }
}
