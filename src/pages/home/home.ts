import { Component, OnInit, NgZone } from '@angular/core';
import { NavController, MenuController, ToastController, ModalController, LoadingController, PopoverController, Platform } from 'ionic-angular';
import { Network } from '@ionic-native/network';

import { Paho } from 'ng2-mqtt/mqttws31';
import { LoginPage } from '../login/login';
import { TabPage } from '../tab/tab';
import { MainPage } from '../main/main';

import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { MenuPage } from '../menu/menu';
import { OrganizationPage } from '../organization/organization';

import { Subscription } from 'rxjs/Subscription';
import { AddorgPage } from '../addorg/addorg';
import { MqttconnectionProvider } from '../../providers/mqttconnection/mqttconnection';
import { Events } from 'ionic-angular';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { LogoutPage } from '../logout/logout';
import { AddlevelsPage } from '../addlevels/addlevels';
import { AdddeviceqrcodePage } from '../adddeviceqrcode/adddeviceqrcode';
import { AddOrgLevelPage } from '../add-org-level/add-org-level';
declare var BMSClient;
declare var BMSPush;
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  connected: Subscription;
  disconnected: Subscription;
  levelHierarchy = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/users/me';
  apiAddUrlorgLevel = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/levels';
  data_hierarchy;
  payload: any;
  organiz;
  client: Paho.MQTT.Client;
  level: any;

  sec: any;
  myLevels: any;
  subsmessage: any[] = [];
  org: any;
  password: any;
  _id: any;
  number: any;
  // tes :any;
  rootPage: HomePage;
  pages: { title: string; component: any; }[];
  error: string;
  errormessage: any;
  firstlevel: any;
  secondlevel: any;
  constructor(public platform: Platform, public popoverCtrl: PopoverController, private zone: NgZone, public events: Events, public toastCtrl: ToastController, private http: Http, public loadingCtrl: LoadingController, public MQTTSERVICE: MqttconnectionProvider, private nativePageTransitions: NativePageTransitions, public navCtrl: NavController, public menu: MenuController, private network: Network, private toast: ToastController, public modalCtrl: ModalController) //private http: HTTP
  {
    this.menu.enable(true);
    this.network.onConnect().subscribe(data => {
      console.log(data)
    }, error => console.error(error));
    this.network.onDisconnect().subscribe(data => {
      console.log(data)
    }, error => console.error(error));
    // ======================================================================= Calling unique id start ================================================================================

    // this.id;

    // ======================================================================= Calling unique id End ================================================================================

    // =======================================================================Calling Connection with broker and subscribe function  start ================================================================================


    // =======================================================================Calling Connection with broker and subscribe function  End ================================================================================
    // console.log(this.tes)
  }
  menus(){
    this.navCtrl.push(MenuPage)
  }
  Addorganiziation() {
    const modal = this.modalCtrl.create(AddorgPage);
    modal.present();
  }
  presentModal() {

  }
  sendEsp() {
    const user = {
      _id: this._id,
      number: this.number,

      // imgPath:this.imgPaths
    }
    console.log(user)
    // var host = "*"
    var win = document.getElementsByTagName('iframe')[0];
    win.contentWindow.postMessage({ user }, "*")
  }
  onbms(){
    this.platform.ready().then(() => {
  

      BMSClient.initialize(".eu-gb.bluemix.net");
      var appGUID = "79da6d54-7660-4c9e-86df-8bf44ddebdde";
      var clientSecret = "c4f999ab-683b-4e7e-a330-120770ab9649";
       var options = {"userId": "shammadahmed31@gmail.com"};//"userId": "shammadahmed31@gmail.com"
      BMSPush.initialize(appGUID,clientSecret,options);
      
      var success = function(response) { 
        console.log("Success: " + response); 
        var token = JSON.parse(response)
        console.log("Success: " + token); 
      };
      var failure = function(response) { 
        console.log("Error: " + response); 
        var token = JSON.parse(response)
        console.log("Error: " + token); 
      };
      // var options = {};
      BMSPush.registerDevice(options, success, failure);
      
      // Register device for push notification without UserId
       
      // BMSPush.registerDevice(options, success, failure);
      
         
         
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
      
       
          });
  }
  ngOnInit() {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    loading.present();
    this.firstlevel = JSON.parse(localStorage.getItem('firstlevel'))
    loading.dismiss();
    this.events.subscribe('user:loggedin', () => {
      this.pages = [
        { title: 'Addorg', component: AddorgPage },
        { title: 'Esp', component: TabPage },
        { title: 'Logout', component: LogoutPage },
      ];
    });
    this.events.subscribe('user:loggedout', () => {
      this.pages = [
        { title: 'AddLevel', component: AddlevelsPage },
        { title: 'Add device', component: AdddeviceqrcodePage },
        { title: 'Logout', component: LogoutPage },
      ];
    });
    this.events.subscribe('user:addorglevel', () => {
      this.pages = [
        { title: 'AddLevel', component: AddOrgLevelPage },
        { title: 'Add device', component: AdddeviceqrcodePage },
        { title: 'Logout', component: LogoutPage },
      ];
    });
    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.levelHierarchy, options)
      .subscribe(data => {
        data.json()
        this.level = data.json()
        this.level = this.level['levels']
        console.log(this.level)
        localStorage.setItem('firstlevel', JSON.stringify(this.level))
        console.log(JSON.parse(localStorage.getItem('firstlevel')))
        this.firstlevel = JSON.parse(localStorage.getItem('firstlevel'))
      }, (err) => {
        console.log(JSON.stringify(err));
        this.error = JSON.parse(JSON.stringify(err))
        console.log(this.error['status']);
        if (this.error['status'] == 401) {
          let toast = this.toastCtrl.create({
            message: `Unauthorized`,
            duration: 2000
          });
          toast.present();
        } else if (this.error['status'] == 0) {
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
        this.errormessage = JSON.parse(this.error['_body'])
        this.errormessage.message
      });
  }
  showsubLevel(level) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    loading.present();
    console.log(level.levelId, 'id')
    localStorage.setItem("org_id", level.levelId)
    this.organiz = level.levelId
    console.log(level)
   setTimeout(() => {
    this.navCtrl.push(OrganizationPage)
    loading.dismiss();
   }, 1000);
      this.apicall();
  }
  apicall() {
    var user = JSON.parse(localStorage.getItem('user'));
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.apiAddUrlorgLevel + "/" + this.organiz + "/hierarchy", options)
      .subscribe(data => {
        console.log(data)
        this.myLevels = data.json(); // data received by server
        localStorage.setItem('secondlevel', JSON.stringify(this.myLevels))
        console.log(JSON.parse(localStorage.getItem('secondlevel')))
      }, (error) => {
        console.log(error.status, 0);
        if (error.status == 0) {
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
        else {
          let toast = this.toastCtrl.create({
            message: error.error.message,
            duration: 2000
          });
          toast.present()
          console.log(error.error, 1, error.error.message, 2); // error message as string
          console.log(error.headers, 3);
        }
      });
  }
  // ======================================================================= loguot function start================================================================================
  logout() {
    localStorage.clear();
    let options: NativeTransitionOptions = {
      direction: 'left',
      duration: 400,
      slowdownfactor: -1,
      iosdelay: 50
    };

    this.nativePageTransitions.slide(options);
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, animation: 'transition', duration: 500, direction: 'forward' });
  }
  // ======================================================================= loguot function End================================================================================
ionViewDidEnter() {
    // this.network.onConnect().subscribe(data => {
    //   console.log(data)
    //   this.displayNetworkUpdate(data.type);
    // }, error => console.error(error));

    // this.network.onDisconnect().subscribe(data => {
    //   console.log(data)
    //   this.displayNetworkUpdate(data.type);
    // }, error => console.error(error));
  }
  displayNetworkUpdate(connectionState: string) {
    // let networkType = this.network.type;
    /*  this.toast.create({
       message: `You are now ${connectionState} via ${networkType}`,
       duration: 3000
     }).present(); */
  }
  doRefresh(refresher) {
    this.firstlevel = JSON.parse(localStorage.getItem('firstlevel'))
    console.log('Begin async operation', refresher);
    this.events.publish('updateScreen');
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  ionViewDidLoad() {
    this.events.publish('user:loggedin');
    // this.navCtrl.setRoot(DashboardPage);
    this.org = true
    localStorage.setItem("orghideshow", this.org)
  }
  ionViewCanLeave() {
    this.events.publish('user:addorglevel');
    this.org = false
    localStorage.setItem("orghideshow", this.org)
  }
  ionViewDidLeave() {
    console.log('view did leave');
  }
  home() {
    this.navCtrl.push(HomePage)
  }
  settings() {
    const modal = this.modalCtrl.create(MenuPage);
    modal.present();
  }
  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage);
    popover.present({
      ev: myEvent
    });
  }
}
