import { Component, OnDestroy } from '@angular/core';
import { NavController, NavParams, ModalController, Platform, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { OfficedevicesPage } from '../officedevices/officedevices';
import { AppliancePage } from '../appliance/appliance';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AddDevicesPage } from '../add-devices/add-devices';
import { DrcodereaderPage } from '../qrcode/drcodereader/drcodereader';
// import { HTTP } from '@ionic-native/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Hotspot } from '@ionic-native/hotspot';
import { HomePage } from '../home/home';
import { MenuPage } from '../menu/menu';
import { ApiStarterPage } from '../api-starter/api-starter';


declare var WifiWizard;


@Component({
  selector: 'page-devices',
  templateUrl: 'devices.html',
})
export class DevicesPage {
  id: any;
  pass: any;
  str: any;

  data_hierarchy: any;
  organization: any;
  level: any;
  // devices = 'https://linked-things-events.eu-gb.mybluemix.net/api/v1/levels/';
  devices = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/levels/'
  _id: any;
  events: any;
  check: any;
  samples: Array<any> = [];
  selectedProduct: any;
  loader: any;
  Device: any;
  deviceHub: any;
  device_Id: any;
  loading: any;

  constructor(private alertCtrl: AlertController,public loadingCtrl: LoadingController, private http: Http, private toast: ToastController, private hotspot: Hotspot, private barcodeScanner: BarcodeScanner, platform: Platform, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
    this.data_hierarchy = this.navParams.data
    console.log(this.data_hierarchy._id)
    this.device_Id = this.data_hierarchy._id
  }
  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Low battery',
      subTitle: '10% of battery remaining',
      buttons: ['Dismiss']
    });
    alert.present();
  }
  menus(){
    this.navCtrl.push(MenuPage)
  }
  ngOnInit() {
    this.loader = this.loadingCtrl.create({
      content: `Please wait...`,
    });
    this.loader.present();
    this.deviceHub = JSON.parse(localStorage.getItem('deviceHub'))
    console.log(this.deviceHub)
    this.loader.dismiss();


    console.log(this.samples);
  }

  showsubLevel(data_hierarchy) {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
      
    });
    this.loading.present();
    this._id = data_hierarchy._id
    console.log(this._id)

    if (data_hierarchy['levels']) {
      console.log(data_hierarchy['levels']);
      this.loading.dismiss();
      this.navCtrl.push(OfficedevicesPage, data_hierarchy['levels'])
   
    }

    else {
      console.log(data_hierarchy);
      this.loading.dismiss();
      this.navCtrl.push(AppliancePage, data_hierarchy)
      
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DevicesPage');
  }

  go() {
    // const modal = this.modalCtrl.create(AddDevicesPage, this._id);
    // modal.present();
    this.navCtrl.push(AddDevicesPage)
  }
  home() {
    this.navCtrl.push(ApiStarterPage)
  }

  AddDevice() {
    this.barcodeScanner.scan().then((barcodeData) => {
      this.selectedProduct = barcodeData;
      console.log(this.selectedProduct.text.split(','))
      this.str = this.selectedProduct.text.split(',')
      var ssid = this.str[0].toString()
      var ssps = this.str[1].toString()
      this.id = atob(ssid)
      this.pass = atob(ssps)
      console.log(this.id);
      console.log(this.pass);
      const espwifi = {
        id: this.id,
        pass: this.pass
      }
      localStorage.setItem("espwifissid", this.id)
      localStorage.setItem("espwifipass", this.pass)
      this.hotspot.connectToWifiAuthEncrypt(this.id, this.pass, "OPEN", ["CCMP", "TKIP", "WEP40", "WEP104"]).then(data => {
        console.log('Connected to User');
        // display alert here
        // alert(data)
        // let toast = this.toast.create({
        //   message: "change wifi ",
        //   duration: 2000
        // });
        // toast.present();
        let alert = this.alertCtrl.create({
          title: 'Alret',
          subTitle: 'Box connected',
          buttons: ['ok']
        });
        alert.present();
        this.go()
        // toast.dismiss();
      }).catch(err => {

        let toast = this.toast.create({
          message: err,
          duration: 8000
        });
        toast.present();
  
        console.log(err)
      });
      console.log("password", this.id, this.pass);
    }).catch(error => {
      alert(JSON.parse(error))
      console.log(error);
      console.log(error.error); // error message as string
      console.log(error.headers);
    });
  }
}
