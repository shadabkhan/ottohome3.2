import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ModalController, LoadingController, Events, MenuController } from 'ionic-angular';
import { DevicesPage } from '../devices/devices';
import { OfficedevicesPage } from '../officedevices/officedevices';
import { AppliancePage } from '../appliance/appliance';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Network } from '@ionic-native/network';
import { Subscription } from 'rxjs/Subscription';
import { AddlevelsPage } from '../addlevels/addlevels';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { HTTP } from '@ionic-native/http';
import { MainPage } from '../main/main';
import { AddDevicesPage } from '../add-devices/add-devices';
import { AddOrgLevelPage } from '../add-org-level/add-org-level';
import { HomePage } from '../home/home';
import { MenuPage } from '../menu/menu';
import { ApiStarterPage } from '../api-starter/api-starter';


@Component({
  selector: 'page-organization',
  templateUrl: 'organization.html',
})
export class OrganizationPage {
  level: any;
  connected: Subscription;
  disconnected: Subscription;
  data_hierarchy: any;
  hierarchy: any;
  _id: any;
  devices = 'https://linked-things-events.eu-gb.mybluemix.net/api/v1/levels/';
  events: any;
  levelId: any;
  loading: any;
  hierarchyLevel: any;
  thirdlevel: any;

  constructor( public menu: MenuController,public even:Events,private http: HTTP, private nativePageTransitions: NativePageTransitions, public loadingCtrl: LoadingController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private network: Network) {
  this.thirdlevel= JSON.parse(localStorage.getItem('secondlevel'))
  console.log(this.thirdlevel['levels']);
  this.thirdlevel= this.thirdlevel['levels']
  console.log(this.thirdlevel);
    this.menu.enable(true);
    this.levelId =  localStorage.getItem('levelId')
  }
  menus(){
    this.navCtrl.push(MenuPage)
  }
  backBtn() {
    this.navCtrl.pop()
  }
  ionViewDidLoad() {
    this.even.publish('user:addorglevel');
    console.log('ionViewDidLoad OrganizationPage');
  }
  ionViewCanLeave() {
    this.even.publish('user:loggedout');
  }
  showsubLevel(data_hierarchy) {
    this._id = data_hierarchy._id
    console.log(data_hierarchy, 'obj')
    console.log(this._id, 'id')
    localStorage.setItem("levelId", this._id)
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    this.loading.present();
    if (data_hierarchy['levels']) {
      this.loading.dismiss();
      console.log(data_hierarchy['levels']);
      localStorage.setItem("fourthlevel",JSON.stringify(data_hierarchy['levels']))
      this.navCtrl.push(OfficedevicesPage, data_hierarchy['levels'])
    } else if (data_hierarchy['devices']) {
      localStorage.setItem("deviceslevel",JSON.stringify(data_hierarchy['devices']))
      console.log(data_hierarchy['devices']);
      this.loading.dismiss();
      this.navCtrl.push(DevicesPage, data_hierarchy)
    }
     else {
      console.log(data_hierarchy, 'before nav');
      this.loading.dismiss();
      this.navCtrl.push(MainPage, data_hierarchy)
    } 
  }
  AddLevel() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
/*     const modal = this.modalCtrl.create(AddOrgLevelPage, this.levelId._id);
    modal.present(); */
    this.navCtrl.push(AddOrgLevelPage)
  }
  AddDevice() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
    // const modal = this.modalCtrl.create(AddDevicesPage, this.levelId._id);
    // modal.present();
    this.navCtrl.push(AddDevicesPage)

  }
  ionViewDidEnter() {
    this.network.onConnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));

    this.network.onDisconnect().subscribe(data => {
      console.log(data)
      this.displayNetworkUpdate(data.type);
    }, error => console.error(error));
  }
  displayNetworkUpdate(connectionState: string) {
    let networkType = this.network.type;
    this.toast.create({
      message: `You are now ${connectionState} via ${networkType}`,
      duration: 3000
    }).present();
  }
  home(){
    this.navCtrl.push(ApiStarterPage)
  }
}
