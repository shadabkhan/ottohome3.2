import { Component, ViewChild, OnDestroy, OnInit, ElementRef } from '@angular/core';
import { NavController, NavParams, ToastController, Slides, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as moment from '../../../node_modules/moment';
import { MqttconnectionProvider } from '../../providers/mqttconnection/mqttconnection';
import { Subscription } from 'rxjs/Subscription';

import * as $ from "jquery";
import * as ibmiotf from 'IBMIoTF'
import { HomePage } from '../home/home';
import { MenuPage } from '../menu/menu';

declare var Chart;
declare var d3v3min
declare var liquidFillGauge
@Component({
  selector: 'page-appliance',
  templateUrl: 'appliance.html',
})
export class AppliancePage implements OnInit, OnDestroy {
  loadLiquidFillGauge(arg0: any, arg1: any): any {
    throw new Error("Method not implemented.");
  }
  liquidFillGaugeDefaultSettings(): any {
    throw new Error("Method not implemented.");
  }
  dateJointTimemotorStatus: any;
  rettimejointDatemotorStatus: Array<any> = [];
  OverheaddeviceData: any;
  UndergrounddeviceData: any;
  jointDateWaterOverhead: any;
  retDateWaterOverhead: any[];
  dateWaterOverhead: Array<any> = [];
  jointDate: any;
  jointTime: any;
  dateWater: Array<any> = [];
  retDateUnderground: Array<any> = [];
  DateUnderground: any;
  timeUnderground: any;
  loader: any;

  @ViewChild('chart') el;
  payloadData: any;
  deviceCompareEvents: any;
  todayYear: string;
  todaymonth: string;
  todaydate: string;
  todayHr: string;
  todayMn: string;
  todayHour: number;
  todayHourAdd5: number;
  todayMnsec: string;
  donut: any;
  motorStatusChart: any;
  motorStatusCreated: any;
  motorStatusValue: any;
  motordate;
  motortoday: string;
  jsmotortodaydate: Array<any> = [];
  messageEvent: string;
  timeWater: Array<any> = [];
  time: Array<any> = [];
  lateChipColor: any;
  rettimeWater: Array<any> = [];
  deviceIds: any;
  Overhead: any;
  Underground: any;
  createdOverhead: any;
  valueOverhead: any;
  retvalueOverhead: any[];
  UndergroundValue: any;
  createdUnderground: any;
  retcreatedUnderground: any[];
  retUndergroundValue: any[];
  timeWaterOverhead: Array<any> = [];
  rettimeWaterOverhead: Array<any> = [];
  timejointTimemotorStatus: Array<any> = [];
  rettimejointTimemotorStatus: Array<any> = [];
  retmotorStatusValue: any[];
  gaugeValue: any;
  hubId(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  check: any;
  data_hierarchy: any;
  _id: any;
  created: any;
  level: any;
  levelId: any;
  createdBy: any;
  levelsLimit: any;
  name: any;
  schema: any;
  type: any;
  updated: any;
  _rev: any;
  value: number;
  devices = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/devices/';
  events: any;
  deviceId: any;
  charts: any;
  ttdtdt: any;
  temp_max: any;
  jsdate: Array<any> = [];
  ionicdate;
  today;
  todays: string;
  emptyspace: number;
  char: any;
  idHub: string;
  id = this.guid();
  connected: Subscription;
  disconnected: Subscription;
  payload: any;
  single: Array<any> = [];
  public buttonClicked: boolean;
  @ViewChild(Slides) slides: Slides;
  percentageValue: (value: number) => string;
  gaugeValues: any = {
  };
  gaugeValuesUnderground: any = {
  };
  interval: any;
  constructor(public loadingCtrl: LoadingController, private http: Http, public toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, public mqttservice: MqttconnectionProvider) {
    this.percentageValue = function (value: number): string {
      return `${Math.round(value)} / ${this['max']}`;
    };

    this.loader = this.loadingCtrl.create({
      content: `Please wait...`,
    });
    this.loader.present();
    this.check = this.navParams.data
    console.log(this.check);
    this.type = this.navParams.data['type'];

    this._id = this.navParams.data['_id'];
    this.deviceIds = this.navParams.data['devices'];
    this.name = 'Overhead'
    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.devices + this.deviceIds[0] + "/events?type=WaterTank_Status", options)
      .subscribe(data => {
        this.loader.dismiss();
        this.Overhead = data.json();
        console.log(this.Overhead)
        localStorage.setItem('OverheaddeviceData', JSON.stringify(this.Overhead))
        console.log(JSON.parse(localStorage.getItem('OverheaddeviceData')))
      }, (error) => {
        this.loader.dismiss();
        console.log(error)
        console.log(error)
        console.log(error.status, 1);
        if (error.status == 0) {
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
        else {
          // console.log(this.checkError.data[0].message);
          this.messageEvent = `Event not fired`
          let toast = this.toastCtrl.create({
            message: `Event not fired`,
            duration: 2000
          });
          toast.present();
        }
      })
    this.http.get(this.devices + this.deviceIds[1] + "/events?type=WaterTank_Status", options)
      .subscribe(data => {
        this.loader.dismiss();
        this.Underground = data.json();
        console.log(this.Underground)
        localStorage.setItem('UndergrounddeviceData', JSON.stringify(this.Underground))
        console.log(JSON.parse(localStorage.getItem('UndergrounddeviceData')))
        console.log(this.Underground)
        this.UndergroundValue = this.Underground.map(Underground => Underground.value)
        this.createdUnderground = this.Underground.map(Underground => Underground.created)
        console.log(this.createdUnderground)
        if (!this.UndergroundValue) {
          this.Underground[0].value = 0
          this.emptyspace = 100 - this.Underground[0].value
        }
        else {
          this.emptyspace = 100 - this.Underground[0].value
        }
      }, (error) => {
        this.loader.dismiss();
        console.log(error)
        console.log(error)
        console.log(error.status, 1);
        if (error.status == 0) {
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
        else {
          // console.log(this.checkError.data[0].message);
          this.messageEvent = `Event not fired`
          let toast = this.toastCtrl.create({
            message: `Event not fired`,
            duration: 2000
          });
          toast.present();
        }
      })
    this.http.get(this.devices + this.deviceIds[0] + "/events?type=Motor_Status", options)
      .subscribe(data => {
        this.loader.dismiss();
        data.json();
        this.motorStatusChart = data.json();
        this.motorStatusValue = this.motorStatusChart.map(motorStatusChart => motorStatusChart.value)
        this.motorStatusCreated = this.motorStatusChart.map(motorStatusChart => motorStatusChart.created)
        console.log(this.motorStatusCreated)
        /*  console.log(this.motorStatusValue)
       console.log(this.motorStatusChart) */
        console.log(this.motorStatusChart)
        console.log(this.motorStatusValue)
        // console.log(this.motorStatusValue[0])
        /*    if (this.motorStatusValue[0] === 1) {
             this.lateChipColor = 'rgb(204, 0, 51)';
           }
           else if (this.motorStatusValue[0] === 0) {
             this.lateChipColor = '#343434';
           } */
        for (let eachMotorStatus of this.motorStatusCreated) {
          let mnt = eachMotorStatus.split('T')[1]
          let jointTimemotorStatus = mnt;
          console.log("final", jointTimemotorStatus);
          this.timejointTimemotorStatus.push(jointTimemotorStatus)
        }
        console.log("year", this.timejointTimemotorStatus);
        this.retmotorStatusValue = new Array;
        for (var i = this.motorStatusValue.length - 1; i >= 0; i--) {
          this.retmotorStatusValue.push(this.motorStatusValue[i]);
        }
        console.log("value", this.retmotorStatusValue)

        this.rettimejointTimemotorStatus = new Array;
        for (var i = this.timejointTimemotorStatus.length - 1; i >= 0; i--) {
          this.rettimejointTimemotorStatus.push(this.timejointTimemotorStatus[i]);
        }
        console.log("Time", this.rettimejointTimemotorStatus)

        new Chart(document.getElementById("line-chart-motor"), {
          type: 'bar',
          data: {
            labels: this.rettimejointTimemotorStatus,
            datasets: [
              {
                data: this.retmotorStatusValue,

                borderColor: "#CC0033",
                fill: true,
                hoverBackgroundColor: "#000000",
                borderWidth: 1
              }
            ]
          },
          options: {
            responsive: true,
            legend: {
              display: false,
            },
            hover: {
              mode: 'label'
            },
            scales: {
              /*    xAxes: [{

                         scaleLabel: {
                             display: false,

                         }
                     }], */
              yAxes: [{
                display: true,
                ticks: {
                  beginAtZero: true,
                  steps: 1,
                  stepValue: 5,
                  max: 1,
                  maxTicksLimit: 1
                }
              }]
            },
            title: {
              display: true

            }
          }
        });

        Chart.defaults.global.elements.line.fill = true;
      }, (error) => {
        this.loader.dismiss();
        console.log(error)
        console.log(error)
        console.log(error.status, 1);
        if (error.status == 0) {
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
        else {
          // console.log(this.checkError.data[0].message);
          let toast = this.toastCtrl.create({
            message: `Event not fired`,
            duration: 2000
          });
          toast.present();
        }
      })
    //overhead motor


    this.http.get(this.devices + this.deviceIds[1] + "/events?type=Motor_Status", options)
      .subscribe(data => {
        this.loader.dismiss();
        data.json();
        this.motorStatusChart = data.json();
        this.motorStatusValue = this.motorStatusChart.map(motorStatusChart => motorStatusChart.value)
        this.motorStatusCreated = this.motorStatusChart.map(motorStatusChart => motorStatusChart.created)
        console.log(this.motorStatusCreated)
        /*  console.log(this.motorStatusValue)
       console.log(this.motorStatusChart) */
        console.log(this.motorStatusChart)
        console.log(this.motorStatusValue)
        // console.log(this.motorStatusValue[0])
        /*    if (this.motorStatusValue[0] === 1) {
             this.lateChipColor = 'rgb(204, 0, 51)';
           }
           else if (this.motorStatusValue[0] === 0) {
             this.lateChipColor = '#343434';
           } */
        for (let eachMotorStatus of this.motorStatusCreated) {
          let mnt = eachMotorStatus.split('T')[1]
          let date = eachMotorStatus.split('T')[0]
          let jointTimemotorStatus = mnt;
          let jointTimemotorStatusdate = date;
          console.log("final", jointTimemotorStatus);
          this.timejointTimemotorStatus.push(jointTimemotorStatus)
          this.dateJointTimemotorStatus.push(jointTimemotorStatusdate)
        }
        console.log("year", this.timejointTimemotorStatus);
        //value
        let retmotorStatusValue = new Array;
        for (var i = this.motorStatusValue.length - 1; i >= 0; i--) {
          this.retmotorStatusValue.push(this.motorStatusValue[i]);
        }
        console.log("value", this.retmotorStatusValue)
        //date
        let rettimejointDatemotorStatus = new Array;
        for (var i = this.timejointTimemotorStatus.length - 1; i >= 0; i--) {
          this.rettimejointDatemotorStatus.push(this.timejointTimemotorStatus[i]);
        }
        console.log("Date", this.rettimejointDatemotorStatus)
        //time
        let rettimejointTimemotorStatus = new Array;
        for (var i = this.dateJointTimemotorStatus.length - 1; i >= 0; i--) {
          this.rettimejointTimemotorStatus.push(this.dateJointTimemotorStatus[i]);
        }
        console.log("Time", this.rettimejointTimemotorStatus)

        new Chart(document.getElementById("line-chart-motor1"), {
          type: 'bar',
          data: {
            labels: this.rettimejointTimemotorStatus,
            datasets: [
              {
                data: this.retmotorStatusValue,

                borderColor: "#CC0033",
                fill: true,
                hoverBackgroundColor: "#000000",
                borderWidth: 1
              }
            ]
          },
          options: {
            responsive: true,
            legend: {
              display: false,
            },
            hover: {
              mode: 'label'
            },
            scales: {
              /*    xAxes: [{
                         scaleLabel: {
                             display: false, }
                     }], */
              yAxes: [{
                display: true,
                ticks: {
                  beginAtZero: true,
                  steps: 1,
                  stepValue: 5,
                  max: 1,
                  maxTicksLimit: 1
                }
              }]
            },
            title: {
              display: true

            }
          }
        });

        Chart.defaults.global.elements.line.fill = true;
      }, (error) => {
        this.loader.dismiss();
        console.log(error)
        console.log(error)
        console.log(error.status, 1);
        if (error.status == 0) {
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
        else {
          // console.log(this.checkError.data[0].message);
          let toast = this.toastCtrl.create({
            message: `Event not fired`,
            duration: 2000
          });
          toast.present();
        }
      })

    var appClientConfig = {
      "org": "2uty47",
      "id": this.id,
      "auth-key": "a-2uty47-gjfzebtgtf",
      "auth-token": "TDrd8kQthdW5ypiZHo"
    };
    var appClient = new ibmiotf.IotfApplication(appClientConfig);

    appClient.connect();

    appClient.on("connect", function (connect) {
      console.log("connect : " + connect);
      /*  appClient.subscribeToDeviceEvents('Water');
       appClient.subscribeToDeviceEvents('Hub'); */
      /*  appClient.subscribeToDeviceEvents('Power'); */
      console.log(localStorage.getItem('typedevice'))

      appClient.subscribeToDeviceEvents('Server');


    });
    appClient.on("deviceEvent", (deviceType, deviceId, eventType, format, payload) => {
      console.log(eventType);

      console.log(this.deviceId)
      this.payloadData = JSON.parse(payload)
      console.log(this.payloadData);
      console.log(this.OverheaddeviceData[0]._id);
      if (this.OverheaddeviceData[0]._id == this.payloadData.deviceId) {
        console.log(deviceId);
        console.log(this.payloadData.value)
        this.Overhead[0].value = this.payloadData.value
        console.log(this.Overhead[0].value);
        // this.emptyspace = 100 - this.Overhead[0].value
        this.todayYear = new Date().toISOString().split(this.created)[0].substring(0, 4);
        this.todaymonth = new Date().toISOString().split(this.created)[0].substring(5, 7);
        this.todaydate = new Date().toISOString().split(this.created)[0].substring(8, 10);
        this.todayHr = new Date().toISOString().split(this.created)[0].substring(11, 13);
        this.todayMn = new Date().toISOString().split(this.created)[0].substring(14, 16);
        this.todayMnsec = new Date().toISOString().split(this.created)[0].substring(17, 19);
        console.log(this.todayYear)
        console.log(this.todaymonth)
        console.log(this.todaydate)
        console.log(this.todayMn)
        console.log(this.todayMnsec)
        this.todayHour = parseInt(this.todayHr)

        this.todayHourAdd5 = this.todayHour
        console.log(this.todayHourAdd5)
        console.log(this.Overhead[0].value)

        const updateValues1 = (): void => {

          this.gaugeValues = {
            1: this.Overhead[0].value

          };
        };
        this.interval = setInterval(updateValues1, 5000);
        updateValues1();
      }
      console.log(this.Underground[0]._id);
      if (this.Underground[0]._id == this.payloadData.deviceId) {
        console.log(deviceId);
        console.log(this.payloadData)
        this.Underground[0].value = this.payloadData.value
        console.log(this.Underground[0].value);
        // this.emptyspace = 100 - this.Underground[0].value
        this.todayYear = new Date().toISOString().split(this.created)[0].substring(0, 4);
        this.todaymonth = new Date().toISOString().split(this.created)[0].substring(5, 7);
        this.todaydate = new Date().toISOString().split(this.created)[0].substring(8, 10);
        this.todayHr = new Date().toISOString().split(this.created)[0].substring(11, 13);
        this.todayMn = new Date().toISOString().split(this.created)[0].substring(14, 16);
        this.todayMnsec = new Date().toISOString().split(this.created)[0].substring(17, 19);
        console.log(this.todayYear)
        console.log(this.todaymonth)
        console.log(this.todaydate)
        console.log(this.todayMn)
        console.log(this.todayMnsec)
        this.todayHour = parseInt(this.todayHr)
        this.todayHourAdd5 = this.todayHour
        console.log(this.Underground[0].value)
        const updateValues = (): void => {
          this.gaugeValuesUnderground = {
            1: this.Underground[0].value
          };
        };
        const INTERVAL: number = 5000;
        this.interval = setInterval(updateValues, INTERVAL);
        updateValues();
      }
    });
    appClient.on("error", function (err) {
      console.log("Error : " + err);
    });
  }
  menus(){
    this.navCtrl.push(MenuPage)
  }
  publishedEvent() {
    this.value = this.motorStatusValue[0]
    if (this.value === 1) {
      this.lateChipColor = 'rgb(204, 0, 51)';
    }
    else if (this.value === 0) {
      this.lateChipColor = '#343434';
    }
    console.log(this.value)
    console.log(this.buttonClicked = !this.buttonClicked)

    if (this.buttonClicked == false) {
      this.value = 0
    }
    else if (this.buttonClicked == true) {
      this.value = 1
    }
    console.log(this.value)
    var appClientConfig = {
      "org": "2uty47",
      "id": this.id,
      "auth-key": "a-2uty47-gjfzebtgtf",
      "auth-token": "TDrd8kQthdW5ypiZHo"
    };
    var appClient = new ibmiotf.IotfApplication(appClientConfig);
    appClient.connect();
    appClient.on("connect", () => {
      //publishing event using the default quality of service
      var data = {
        deviceId: this._id,
        hubId: this.hubId,
        value: this.value,
        type: "Motor_Status",
        createdBy: localStorage.getItem('userEmail')
      }
      appClient.publishDeviceCommand("Water", this.hubId, "AppEvents", "json", data);
    });
  }
  guid() {
    let date = new Date();

    let str = date.getFullYear() + '' + date.getMonth() + '' + date.getDate() + '' + date.getHours() + '' + date.getMinutes() + '' + date.getSeconds();
    console.log(str)
    return str;
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AppliancePage');
  }
  goToSlide() {
    let currentIndex = this.slides.getActiveIndex();
    // alert('Current index is'+ currentIndex);
    if (currentIndex == 0) {
      this.name = 'Overhead'
    } else if (currentIndex == 1) {
      this.name = 'Underground'
    }
  }
  toggle(id) {
    $('#' + id).slideToggle();
    let card1_visibility = $("#card1").is(":visible");
    let card2_visibility = $("#card2").is(":visible");
    let card3_visibility = $("#card3").is(":visible");
    let card4_visibility = $("#card4").is(":visible");
    let card5_visibility = $("#card5").is(":visible");
    let card6_visibility = $("#card6").is(":visible");
    let card7_visibility = $("#card7").is(":visible");
    let card8_visibility = $("#card8").is(":visible");
    if (id === 'card2') {
      // if (card1_visibility) $('#card1').slideToggle();
      if (card3_visibility) $('#card3').slideToggle();
      if (card4_visibility) $('#card4').slideToggle();
    }
    if (id === 'card3') {
      // if (card1_visibility) $('#card1').slideToggle();
      if (card2_visibility) $('#card2').slideToggle();
      if (card4_visibility) $('#card4').slideToggle();
    }
    if (id === 'card4') {
      // if (card1_visibility) $('#card1').slideToggle();
      if (card2_visibility) $('#card2').slideToggle();
      if (card3_visibility) $('#card3').slideToggle();
    }
    /*     if (id === 'card5') {
          if (card6_visibility) $('#card6').slideToggle();
          if (card7_visibility) $('#card7').slideToggle();
          if (card8_visibility) $('#card8').slideToggle();
        } */
    if (id === 'card6') {
      // if (card5_visibility) $('#card5').slideToggle();
      if (card7_visibility) $('#card7').slideToggle();
      if (card8_visibility) $('#card8').slideToggle();
    }
    if (id === 'card7') {
      // if (card5_visibility) $('#card5').slideToggle();
      if (card6_visibility) $('#card6').slideToggle();
      if (card8_visibility) $('#card8').slideToggle();
    }
    if (id === 'card8') {
      // if (card5_visibility) $('#card5').slideToggle();
      if (card6_visibility) $('#card6').slideToggle();
      if (card7_visibility) $('#card7').slideToggle();
    }
  }
  ngOnInit(): void {
  /*   new liquidFillGauge(document.getElementById("fillgauge1"), {
    config4 = liquidFillGaugeDefaultSettings

      config4.circleThickness = 0.1,
      config4.circleColor = "rgba(255, 255, 255, 0.3)",
      config4.textColor = "#fff",
      config4.waveTextColor = "#fff",
      config4.waveColor = "rgba(0, 150, 210, 0.5)",
      config4.textVertPosition = 0.5,
      config4.waveAnimateTime = 1000,
      config4.waveHeight = 0.05,
      config4.waveAnimate = true,
      config4.waveRise = false,
      config4.waveHeightScaling = false,
      config4.waveOffset = 0.25,
      config4.textSize = 1,
      config4.waveCount = 2,
      liquidFillGauge.loadLiquidFillGauge("fillgauge1", 75, config4)
    }) */
//  /*    var config4 = liquidFillGauge.liquidFillGaugeDefaultSettings();
//     config4.circleThickness = 0.1;
//     config4.circleColor = "rgba(255, 255, 255, 0.3)";
//     config4.textColor = "#fff";
//     config4.waveTextColor = "#fff";
//     config4.waveColor = "rgba(0, 150, 210, 0.5)";
//     config4.textVertPosition = 0.5;
//     config4.waveAnimateTime = 1000;
//     config4.waveHeight = 0.05;
//     config4.waveAnimate = true;
//     config4.waveRise = false;
//     config4.waveHeightScaling = false;
//     config4.waveOffset = 0.25;
//     config4.textSize = 1;
//     config4.waveCount = 2;
//     liquidFillGauge.loadLiquidFillGauge("fillgauge1", 75, config4); */




    console.log(this.gaugeValue)
      setTimeout(() => {
      this.OverheaddeviceData = JSON.parse(localStorage.getItem('OverheaddeviceData'))


      console.log(this.OverheaddeviceData)
      const updateValues1 = (): void => {
        this.gaugeValues = {
          1: this.OverheaddeviceData[0].value
        };
      };
      this.interval = setInterval(updateValues1, 5000);
      updateValues1();
 
      this.valueOverhead = this.OverheaddeviceData.map(OverheaddeviceData => OverheaddeviceData.value)
      this.createdOverhead = this.OverheaddeviceData.map(OverheaddeviceData => OverheaddeviceData.created)
      console.log(this.createdOverhead)
      for (let eachStatusStatus of this.createdOverhead) {
        let mnt = eachStatusStatus.split('T')[1]
        let date = eachStatusStatus.split('T')[0]
        let jointTimeWaterOverhead = mnt;
        let jointDateWaterOverhead = date;
        console.log("final", jointTimeWaterOverhead);
        this.dateWaterOverhead.push(jointDateWaterOverhead)
        this.timeWaterOverhead.push(jointTimeWaterOverhead)
      }
      this.retDateWaterOverhead = new Array;
      for (var i = this.dateWaterOverhead.length - 1; i >= 0; i--) {
        this.retDateWaterOverhead.push(this.dateWaterOverhead[i]);
      }
      this.retDateWaterOverhead[0]
      console.log(this.retDateWaterOverhead[0])

      this.jointDate = this.retDateWaterOverhead[0]
      this.rettimeWaterOverhead = new Array;
      for (var i = this.timeWaterOverhead.length - 1; i >= 0; i--) {
        this.rettimeWaterOverhead.push(this.timeWaterOverhead[i]);
      }
      /*  console.log(" time ", this.timeWaterOverhead[0]); */
      this.jointTime = this.timeWaterOverhead[0]
      this.retvalueOverhead = new Array;
      for (var i = this.valueOverhead.length - 1; i >= 0; i--) {
        this.retvalueOverhead.push(this.valueOverhead[i]);
      }
      console.log("time", this.rettimeWaterOverhead)
      console.log("Date", this.retDateWaterOverhead);
      console.log("value", this.retvalueOverhead)
      new Chart(document.getElementById("line-chart"), {
        type: 'line',
        data: {
          labels: ["11:07:00.000Z", "11:17:00.000Z", "11:20:00.000Z", "11:27:00.000Z"],
          datasets: [
            {
              data: this.retvalueOverhead,
              label: "",
              borderColor: "#333333",
              fill: true,
              hoverBackgroundColor: "#000000",
              borderWidth: 1
            }, {
              data: [25, 42, 60, 47],
              label: "",
              borderColor: "#8e5ea2",
              fill: true,
              hoverBackgroundColor: "#000000",
              borderWidth: 1
            }
          ]
        },
        options: {
          responsive: true,
          legend: {
            display: false,
          },
          hover: {
            mode: 'label'
          },
          scales: {
            xAxes: [{
              display: true,
              scaleLabel: {
                display: true,

              }
            }],
            yAxes: [{
              display: true,
              ticks: {
                beginAtZero: true,
                steps: 10,
                stepValue: 5,
                max: 100
              }
            }]
          },
          title: {
            display: true,
          }
        }
      });
    }, 1000);
    //  UndergrounddeviceData
    console.log(JSON.parse(localStorage.getItem('UndergrounddeviceData')))
    setTimeout(() => {
      this.UndergrounddeviceData = JSON.parse(localStorage.getItem('UndergrounddeviceData'))


      console.log(this.UndergrounddeviceData)
      console.log(this.UndergrounddeviceData[0].value)
      this.UndergroundValue = this.UndergrounddeviceData.map(UndergrounddeviceData => UndergrounddeviceData.value)
      this.createdUnderground = this.UndergrounddeviceData.map(UndergrounddeviceData => UndergrounddeviceData.created)
      console.log(this.createdUnderground)
      this.UndergrounddeviceData[0].value
      const updateValues = (): void => {
        this.gaugeValuesUnderground = {
          1: this.UndergrounddeviceData[0].value
        };
      };
      const INTERVAL: number = 5000;
      this.interval = setInterval(updateValues, INTERVAL);
      updateValues();
      for (let eachStatusStatus of this.createdUnderground) {
        let mnt = eachStatusStatus.split('T')[1]
        let dat = eachStatusStatus.split('T')[0]
        let jointTimeWater = mnt;
        let jointDateWater = dat;
        console.log("final", jointTimeWater);
        this.timeWater.push(jointTimeWater)
        this.dateWater.push(jointDateWater)
      }
      this.retcreatedUnderground = new Array;
      for (var i = this.timeWater.length - 1; i >= 0; i--) {
        this.retcreatedUnderground.push(this.timeWater[i]);
      }
      console.log("Time", this.retcreatedUnderground)
      this.timeUnderground = this.retcreatedUnderground[0]
      this.retDateUnderground = new Array;
      for (var i = this.dateWater.length - 1; i >= 0; i--) {
        this.retDateUnderground.push(this.dateWater[i]);
      }
      console.log("Date", this.retDateUnderground)
      this.DateUnderground = this.retDateUnderground[0]
      this.retUndergroundValue = new Array;
      for (var i = this.UndergroundValue.length - 1; i >= 0; i--) {
        this.retUndergroundValue.push(this.UndergroundValue[i]);
      }
      console.log("Value", this.retUndergroundValue)

      new Chart(document.getElementById("line-chart2"), {
        type: 'line',
        data: {
          labels: this.retcreatedUnderground,
          datasets: [
            {
              data: this.retUndergroundValue,
              label: "",
              borderColor: "#333333",
              fill: true,
              hoverBackgroundColor: "#000000",
              borderWidth: 1
            }
          ]
        },
        options: {
          responsive: true,
          legend: {
            display: false,
          },
          hover: {
            mode: 'label'
          },
          scales: {
            xAxes: [{
              display: true,
              scaleLabel: {
                display: true,

              }
            }],
            yAxes: [{
              display: true,
              ticks: {
                beginAtZero: true,
                steps: 10,
                stepValue: 5,
                max: 100
              }
            }]
          },
          title: {
            display: true,
          }
        }
      });
    }, 1000);
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }
  ngAfterViewInit() {
    console.log("afterinit");
  }
  backBtn() {
    this.navCtrl.pop()
  }
  home() {
    this.navCtrl.push(HomePage)
  }

}
