import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';
// import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-addorg',
  templateUrl: 'addorg.html',
})
export class AddorgPage {
  name: any;
  type: any;
  org_type: any;
  apiAddUrlorgLevel = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/levels';
  level: any;
  SubCategoryId: any;
  checkError: any;
  _id: any;
  constructor(public loadingCtrl: LoadingController,public toastCtrl: ToastController,public AuthenticateProvider: AuthenticationProvider,public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,private http: Http) {
  }
  Categary() {
    
    console.log(this.SubCategoryId);

  }
submit() {
const org = {
_id: this._id,
name: this.name,
type: this.SubCategoryId,

// imgPath:this.imgPaths

}
let loading = this.loadingCtrl.create({
  content: 'Please wait user login ...',
  enableBackdropDismiss: false
});
loading.present();
var user = JSON.parse(localStorage.getItem('user'));
console.log(user)
let postParams = {
  // "Id": user.id,
  "token": user.token
}
/* var data = JSON.parse(postParams.token)
console.log(JSON.parse(data.data))
var datadata = JSON.parse(data.data)
console.log("here is token", datadata.token) */
let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
let options = new RequestOptions({ headers: headers });
this.http.post(this.apiAddUrlorgLevel,org, options)
.subscribe(data => {
loading.dismiss();
  console.log(data.status);
  console.log(data); // data received by server
  console.log(data.headers);
this.navCtrl.push(HomePage)
},(error) => {
  loading.dismiss();
/*   this.checkError =JSON.parse(error.error)
  this.checkError.message */
  console.log(error);
 // console.log(JSON.parse(error.error)); // error message as string
//   console.log(error.headers);
//   if(this.checkError.message == "409 Conflict"){
//   let toast = this.toastCtrl.create({
//     message:this.checkError.message,
//     duration: 2000
//   });
//   toast.present();
// }else{
//   console.log(this.checkError.data[0].message);
  
//   let toast = this.toastCtrl.create({
//     message: this.checkError.data[0].message,
//     duration: 2000
//   });
//   toast.present();
// }
});
  // v
    // var user = JSON.parse(localStorage.getItem('user'));
    // let postParams = {
    //   // "Id": user.id,
    //   "token": user.token
    // }
    // let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    // let options = new RequestOptions({ headers: headers });
    // return new Promise((resolve, reject) => {
    //   this.http.post(this.apiAddUrlorgLevel,org, options)
    //     .subscribe(data => {
    //       data.json()
    //       this.level = data.json()
    //       console.log(this.level)
    //     }, (err) => {
    //       reject(err);
    //       console.log(err)
    //     });
    // });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddorgPage');
  }
  dismiss() {
    this.viewCtrl.dismiss()
  }
}
