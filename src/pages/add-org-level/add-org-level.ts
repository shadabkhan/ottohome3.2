import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
 import { HomePage } from '../home/home';
/**
 * Generated class for the AddOrgLevelPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-add-org-level',
  templateUrl: 'add-org-level.html',
})
export class AddOrgLevelPage {
  name: any;
  org_type: any;
  levelId: any;
  // apiAddUrlorgLevel = 'https://linked-things-orgs.eu-gb.mybluemix.net/api/v1/levels';
  level: any;
  checkError: any;
  apiAddUrlorgLevel = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/levels';
  SubCategoryId: any;
  _id: any;

  constructor(public loadingCtrl: LoadingController, public toastCtrl: ToastController,  private http: Http, public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.levelId = this.navParams.data
    console.log(this.levelId)
    this.levelId = localStorage.getItem("org_id")
  }
  Categary() {
    
    console.log(this.SubCategoryId);

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddOrgLevelPage');
  }
  submit() {
    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }

    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    loading.present();
  /*   var data = JSON.parse(postParams.token)
    console.log(JSON.parse(data.data))
    var datadata = JSON.parse(data.data)
    console.log("here is token", datadata.token) */
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    const org = {
      _id: this._id,
      name: this.name,
      
      type: this.SubCategoryId,
      levelId: this.levelId,
      // imgPath:this.imgPaths

    }
    this.http.post(this.apiAddUrlorgLevel, org, options)
      .subscribe(data => {
        loading.dismiss();
        console.log(data);
        console.log(data); // data received by server
        this.navCtrl.push(HomePage)
        console.log(data);

      },(error) => {

        loading.dismiss();
        this.checkError = JSON.parse(error.error)
        this.checkError.message
        console.log(error.status);
        console.log(JSON.parse(error.error)); // error message as string
        console.log(error.headers);
        if (this.checkError.message == "409 Conflict") {
          let toast = this.toastCtrl.create({
            message: this.checkError.message,
            duration: 2000
          });
          toast.present();
        } else {
          console.log(this.checkError.data[0].message);

          let toast = this.toastCtrl.create({
            message: this.checkError.data[0].message,
            duration: 2000
          });
          toast.present();
        }

      });
    // var user = JSON.parse(localStorage.getItem('user'));
    // let postParams = {
    //   // "Id": user.id,
    //   "token": user.token
    // }
    // let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    // let options = new RequestOptions({ headers: headers });
    // return new Promise((resolve, reject) => {
    //   this.http.post(this.apiAddUrlorgLevel,org, options)
    //     .subscribe(data => {
    //       data.json()
    //       this.level = data.json()
    //       console.log(this.level)
    //     }, (err) => {
    //       reject(err);
    //       console.log(err)
    //     });
    // });

  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
