import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { MenuPage } from '../menu/menu';
import { OrganizationPage } from '../organization/organization';

@Component({
  selector: 'page-api-starter',
  templateUrl: 'api-starter.html',
})
export class ApiStarterPage implements OnInit{
  apiAddUrlorgLevel = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/levels';

  levelHierarchy = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/users/me';
  level: any;
  firstlevel: any;
  errormessage: any;
  error: any;
  organiz: any;
  myLevels: any;

  constructor(private http: Http,public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController, public loadingCtrl: LoadingController) {
  }

  menu(){
    this.navCtrl.push(MenuPage)
  }
  ngOnInit(){
    this.firstlevel = JSON.parse(localStorage.getItem('firstlevel')) 
    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.levelHierarchy, options)
      .subscribe(data => {
        data.json() 
        console.log( data.json() ) 
        this.level = data.json()
        this.level = this.level['levels']
        console.log(this.level)
        localStorage.setItem('firstlevel', JSON.stringify(this.level))
        console.log(JSON.parse(localStorage.getItem('firstlevel')))
        this.firstlevel = JSON.parse(localStorage.getItem('firstlevel')) 
      }, (err) => {

        console.log(err) 
         console.log(JSON.stringify(err));
        this.error = JSON.parse(JSON.stringify(err))
        console.log(this.error['status']);
        if (this.error['status'] == 401) {
          let toast = this.toastCtrl.create({
            message: `Unauthorized`,
            duration: 2000
          });
          toast.present();
        } else if (this.error['status'] == 0) {
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
/* s */
      });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ApiStarterPage');
  }
  showsubLevel(level) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    loading.present();
    console.log(level.levelId, 'id')
    localStorage.setItem("org_id", level.levelId)
    this.organiz = level.levelId
    console.log(level)
   setTimeout(() => {
    this.navCtrl.push(OrganizationPage)
    loading.dismiss();
   }, 1000);
      this.apicall();
  }
  apicall() {
    var user = JSON.parse(localStorage.getItem('user'));
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }
    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.apiAddUrlorgLevel + "/" + this.organiz + "/hierarchy", options)
      .subscribe(data => {
        console.log(data)
        this.myLevels = data.json(); // data received by server
        localStorage.setItem('secondlevel', JSON.stringify(this.myLevels))
        console.log(JSON.parse(localStorage.getItem('secondlevel')))
      }, (error) => {
        console.log(error.status, 0);
        if (error.status == 0) {
          let toast = this.toastCtrl.create({
            message: `Ehternet not connected`,
            duration: 2000
          });
          toast.present();
        }
        else {
          let toast = this.toastCtrl.create({
            message: error.error.message,
            duration: 2000
          });
          toast.present()
          console.log(error.error, 1, error.error.message, 2); // error message as string
          console.log(error.headers, 3);
        }
      });
  }
}
