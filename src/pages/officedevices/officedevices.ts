import { Component, OnDestroy } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, ToastController } from 'ionic-angular';
import { AppliancePage } from '../appliance/appliance';
import { DevicesPage } from '../devices/devices';
import { DrcodereaderPage } from '../qrcode/drcodereader/drcodereader';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AddDevicesPage } from '../add-devices/add-devices';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Hotspot } from '@ionic-native/hotspot';
import { AddlevelsPage } from '../addlevels/addlevels';
import { MainPage } from '../main/main';
import { AddOrgLevelPage } from '../add-org-level/add-org-level';
import { HomePage } from '../home/home';
import { MenuPage } from '../menu/menu';
import { ApiStarterPage } from '../api-starter/api-starter';
declare var WifiWizard;
@Component({
  selector: 'page-officedevices',
  templateUrl: 'officedevices.html',
})
export class OfficedevicesPage {
  data_hierarchy: any;
  _id: any;
  created: any;
  level: any;
  levelId: any;
  createdBy: any;
  levelsLimit: any;
  name: any;
  schema: any;
  type: any;
  updated: any;
  _rev: any;
  check: Array<any> = [];
  deviceCompareEvents: Array<any> = [];
  devices = 'http://linked-things-microservices-gateway.eu-gb.mybluemix.net/api/v1/levels/'
  
  events: any[];
  checkd: any[];
  newArray: any[];
  selectedProduct: any;
  pass: any;
  id: any;
  str: any;
  deviceId: any;
  value: any;
  hubId: any;
  loading: any;
    lenghtofDevices:   Array<any> = [];
  fourthlevel: string;
  constructor(public toastCtrl: ToastController,private hotspot: Hotspot, private barcodeScanner: BarcodeScanner, private http: Http, private nativePageTransitions: NativePageTransitions, public loadingCtrl: LoadingController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {

  }
  backBtn() {
    this.navCtrl.pop()
  }
  ngOnInit() {
   this.fourthlevel = JSON.parse(localStorage.getItem("fourthlevel"))
   console.log(this.fourthlevel);
    console.log(this.navParams.data);
    this.check = this.navParams.data
    console.log(this.check);
    console.log(this.check);
    this._id = this.navParams.data['_id'];
    this.deviceId = this.navParams.data['deviceId'];
    this.created = this.navParams.data['created'];
    this.createdBy = this.navParams.data['createdBy'];
    this.level = this.navParams.data['level'];
    // this.levelId = this.navParams.data['levelId'];
    this.levelsLimit = this.navParams.data['levelsLimit'];
    this.name = this.navParams.data['name'];
    this.schema = this.navParams.data['schema'];
    this.type = this.navParams.data['type'];
    this.updated = this.navParams.data['updated'];
    this._rev = this.navParams.data['_rev'];
    this.value = this.navParams.data['value'];
    this.hubId = this.navParams.data['hubId'];
    localStorage.setItem('typedevice', this.type)
    console.log(this.hubId)
    console.log(this.type)
    console.log(this.value)
    console.log(this._id)
    console.log(this.type)
    localStorage.setItem("levelId._id", this._id)
    this.events = JSON.parse(localStorage.getItem('DeviceEvent'))
    this.levelId =  localStorage.getItem('levelId')
  }
  menus(){
    this.navCtrl.push(MenuPage)
  }
  showsubLevel(data_hierarchy) {
    this._id = data_hierarchy._id
    localStorage.setItem("levelId", this._id)
    console.log(this._id, 'device level')
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      enableBackdropDismiss: false
    });
    this.loading.present();

    var user = JSON.parse(localStorage.getItem('user'));
    console.log(user)
    let postParams = {
      // "Id": user.id,
      "token": user.token
    }

    let headers = new Headers({ 'Authorization': 'Bearer ' + postParams.token });
    let options = new RequestOptions({ headers: headers });
    this.http.get(this.devices + this._id + "/hubs", options)
      .subscribe(data => {
        this.check = data.json()
        localStorage.setItem('deviceHub', JSON.stringify(this.check))
        console.log(JSON.parse(localStorage.getItem('deviceHub')))
        console.log(data.json()); // data received by server
        console.log(data.headers);
        // this.navCtrl.push(HomePage)
      }, (error) => {


        console.log(error);

      });
    if (data_hierarchy['levels']) {
      console.log(data_hierarchy['levels'],1);
      this.loading.dismiss();
      localStorage.setItem("fourthlevel",JSON.stringify(data_hierarchy['levels']))
      this.navCtrl.push(OfficedevicesPage, data_hierarchy['levels'])


    } else if (data_hierarchy) {
      console.log(data_hierarchy,2);

      this.loading.dismiss();

      this.navCtrl.push(DevicesPage, data_hierarchy)
    }

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad OfficedevicesPage');
  }
  ionViewWillLeave() {
  }
  AddLevel() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
/*     const modal = this.modalCtrl.create(AddlevelsPage, this.levelId._id);
    modal.present(); */
    this.navCtrl.push(AddlevelsPage)
  }
  AddDevice() {
    // this.navCtrl.push(AddlevelsPage, this.levelId._id);
  /*   const modal = this.modalCtrl.create(AddDevicesPage, this.levelId._id);
    modal.present(); */
    this.navCtrl.push(AddDevicesPage)
  }
  home(){
    this.navCtrl.push(ApiStarterPage)
  }
}
