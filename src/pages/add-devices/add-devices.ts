import { Component } from '@angular/core';
import { NavController, NavParams ,ModalController, ViewController} from 'ionic-angular';
import { IframePage } from '../iframe/iframe';



@Component({
  selector: 'page-add-devices',
  templateUrl: 'add-devices.html',
})
export class AddDevicesPage {
  _id: string;
  orgid: string;
  password: any;
  user: any;
  public type = 'password';
  public showPass = false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public viewCtrl: ViewController, public modalCtrl: ModalController) {
    var ssid = localStorage.getItem('ssid')
    let org_id = localStorage.getItem('org_id')
    this._id = ssid
    this.orgid = org_id
  }
  showPassword() {
    this.showPass = !this.showPass;
 
    if(this.showPass){
      this.type = 'text';
    } else {
      this.type = 'password';
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AddDevicesPage');
  }
  wifiIdPass(){
    const user = {
      _id: this._id,
      password: this.password,}
console.log(user.password)
      localStorage.setItem("wifiIdPass",user.password)
      setTimeout(this.go(), 100);
      this.viewCtrl.dismiss()
     
  }
  go(){
/*     const modal = this.modalCtrl.create(IframePage);
    modal.present(); */
    this.navCtrl.push(IframePage)
  }
  dismiss() {
    this.viewCtrl.dismiss()
  }
}
